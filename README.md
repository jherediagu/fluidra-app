Our New Slogan
==
Fluidra inc.
--
### Developed by: **Javajan Serveis d'Internet**


### Built with:
- Angular@2.0.0-rc
- Ionic2 Framework
- Typescript
- crosswalk-webview plugin.

### We use:
- ionic-cli: Ionic Framework Client
- ng-cli: Angular2 Generator Client

----

Aquest projecte s'ha desenvolupat amb una plataforma beta, per tant pot ser que els frameworks utilitzats
s'hagin actualitzat i s'hagi d'adaptar una mica el codi. Només afectaria el cas de Ionic, ja que Angular esta en aquests
moments en un estat de Release Candidate.

*Nota molt important*.

El target al que va instal·lat aquesta app utilitza una versió antiga d'android (4.1.2) per tant hem hagut d'utilitzar
el plugin `corsswalk-webview` per tant de donar-li compatibilitat.

----

**Original developers:**
- Juan Antonio Heredia (joanh@javajan.com)
- Edgar Ordoñez (dev@javajan.com)