import {Injectable} from 'angular2/core';
import {Http, Headers, RequestOptions} from 'angular2/http';
import {User} from '../models/User';

@Injectable()
export class AuthService {
	
	public static SERVER: string = 'http://ournewslogan.fluidra.com/';
	//public static SERVER: string = 'http://212.92.56.234/vhosts/ournewslogan/';
	
	http: Http;
	data: User;

	constructor(http: Http) {
		this.http = http;
	}
	
	
	/**
	 * API PRE-AUTH
	 * ============
	 * Check if the code is on database and retrieve an email user.
	 * 
	 * @param  {string} code
	 */
	preAuth(code: string) {
		return this.http.get(AuthService.SERVER + 'auth/'+code);
	}

	/**
	 * API Auth 
	 * @param {string} username [description]
	 * @param {string} password [description]
	 */
	auth(username: string, password: string) {
		let data = JSON.stringify({ email: username, password: password });
		return this.http.post(AuthService.SERVER + 'auth/login', data);
	}

}