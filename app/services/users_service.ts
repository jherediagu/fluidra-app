import {Device} from 'ionic-native';
import {Http, Headers, RequestOptions} from 'angular2/http';
import {Injectable} from 'angular2/core';
import {User} from '../models/User';
import {TokenFactory} from '../factories/TokenFactory';

@Injectable()
export class UserService {
	
	public static SERVER: string = 'http://ournewslogan.fluidra.com/';
	//public static SERVER: string = 'http://212.92.56.234/vhosts/ournewslogan/';

	http: Http;
	data: User;
	token: string;

	
	constructor(http: Http) {
		this.http = http;
		this.token = TokenFactory.getInstance().getToken();
	}
	
	all() {
		let token_params = '?token=' + this.token;
		return this.http.get(UserService.SERVER + 'users' + token_params);
	}


	findById(id: number) {
		let token_params = '?token=' + this.token;
		
		return this.http.get(UserService.SERVER + 'users/' + id + token_params);
	}
	
	
	/**
	 * User Device
	 * ===========
	 * Send device information to save on User API object 
	 */
	sendUserDeviceInfo() {
		
		let token_params = '?token=' + this.token;
		
		let data = JSON.stringify({
			device_uiid: Device.device.uuid,
			device: Device.device.platform
		});
		 
		return this.http.post(UserService.SERVER + 'profile/device' + token_params, data);
	}
	
	
	/**
	 * recoverPassword
	 * ===============
	 * Send request to recovery process 
	 * 
	 */
	recoverPassword(email: string) {
		let params = '?e=' + email;
		
		return this.http.get(UserService.SERVER + 'lostpassword' + params);
	}
	
	
	
	/**
	 * Save User
	 * =========
	 * PUTS user object to Server API.
	 */
	save(u: User) {
		
		let token_params = '?token=' + this.token;
		// By Default create new user.
		let action = 'users/create';
		
		// This user have id => Update profile
		if(u.id > 0) {
			action = 'profile';
		}
		// Transform data to String
		let data = JSON.stringify(u);
		
		// Send HTTP to API and return a promise object
		return this.http.post(UserService.SERVER + action + token_params, data);
	}
}