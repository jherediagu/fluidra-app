import {Http, Headers, RequestOptions}  from 'angular2/http';
import {Injectable} 					from 'angular2/core';
import {TokenFactory} 					from '../factories/TokenFactory';
import {Claim} 							from '../models/Claim';
import {User} 							from '../models/User';

@Injectable()
export class ClaimService {

	public static SERVER: string = 'http://ournewslogan.fluidra.com/';
	//public static SERVER: string = 'http://212.92.56.234/vhosts/ournewslogan/';


	http: Http;
	data: Array<Claim> = [];
	token: string;
	last_id: number = 0;
	first_id: number = 0;
	http_response; 

	constructor(http: Http) {
		this.http = http;
		this.token = TokenFactory.getInstance().getToken();
	}


	/**
	 * Have a most newer claims
	 * 
	 * @param {number} id
	 * @param {number} limit
	 */
	newerThan(id: number, limit: number) {
		let token_param = '?token=' + this.token;
		let url = 'claims/'+ limit +'/newerThan/' + id;
		
		this.http.get(ClaimService.SERVER + url + token_param).subscribe(response => {
			
			if(response.json().nData > 0) this.http_response = response.json();
			
			if(response.json().data != null && response.json().data.length > 0) {
				// Save the last id 
				this.first_id = response.json().data.first_id;
				this.last_id = response.json().data.last_id;
				
				var items = response.json().data;
				items.forEach(item => {
					this.addClaimToData(item);
				});
			}
			
			console.log(this.data);
		}, error => {

		});
	}


	/**
	 * Have a older posts..
	 */
	olderThan(id: number, limit: number) {
		let token_param = '?token=' + this.token;
		let url = 'claims/'+ limit +'/olderThan/' + id;

		this.http.get(ClaimService.SERVER + url + token_param).subscribe(response => {
			
			//if(response.json().nData > 0) this.http_response = response.json();
			
			if(response.json().data != null && response.json().data.length > 0) {
				// Save the last id 
				this.first_id = response.json().data.first_id;
				this.last_id = response.json().data.last_id;
				
				var items = response.json().data;
				items.forEach(item => {
					this.addClaimToData(item);
				});
			}
			
			console.log(this.data);
		}, error => {

		});
	}
	
	
	/**
	 * getUserClaims
	 * ===
	 * Retrieve all user claims
	 */
	getUserClaims(id?: number) {
		let token_params 	= '?token=' + this.token;
		let action 			= 'users/claims';
		if(id > 0) action = 'users/' + id + '/claims';
		
		return this.http.get(ClaimService.SERVER + action + token_params);
	}
	


	/**
	 * Retrieve paginate claims from API
	 * 
	 * @param  {number} limit?
	 * @param  {number} starts?
	 */
	retrieveData(limit?: number, starts?: number) {
		
		let url = "claims";
		let token_param = '?token=' + this.token;
		
		if (limit > 0) url = "claims/" + limit;
		if (limit > 0 && starts > 0) url = "claims/between/" + starts + '-' + limit;

		this.http.get(ClaimService.SERVER + url + token_param).subscribe(response => {
			
			if(response.json().data != null) this.http_response = response.json();
			
			if(response.json().data != null && response.json().data.length > 0) {
				
				var items = response.json().data;
				items.forEach(item => {
					this.addClaimToData(item);
				});
			}
			
			console.log(this.data);
		}, error => {

		});
	}
	
	transformToClaim(item) {
	
		// Transforms to boolean
		if(item.liked > 0 ) item.liked = true;
		else item.liked = false;
		
		let c = new Claim(item.id, item.likes, item.liked, item.text, item.created_at);
		let u = new User(item.author.id, item.author.name, item.author.surname, item.author.company, item.author.email, item.author.country, true, item.author.picture);
		
		c.setAuthor(u);
		
		return c;
	}
	
	
	private addClaimToData(item) {
		
		// Transforms to boolean
		if(item.liked > 0 ) item.liked = true;
		else item.liked = false;
		
		let c = new Claim(item.id, item.likes, item.liked, item.text, item.created_at);
		let u = new User(item.author.id, item.author.name, item.author.surname, item.author.company, item.author.email, item.author.country, true, item.author.picture);
		
		
		c.setAuthor(u);
		
		
		this.data.push(c);
	}
	
	/**
	 * getTopClaims
	 * 
	 * Return the top 5 
	 */
	getTopClaims() {
		
		let token_param = '?token=' + this.token;
		return this.http.get(ClaimService.SERVER + 'claims/top' + token_param);
		
	}

	/**
	 * Return actual data
	 */
	getData(){
		let claims =  this.data;
		return claims;
		
	}


	
	/**
	 * Send new claim to API
	 * ==
	 * 
	 * @param  {Claim} claim
	 * @param  {any} callback
	 */
	add(claim: Claim, callback) {
		
		if (this.data == null) this.retrieveData();
		// Save this claim on the array object loaded
		console.log("Add new Claim", claim);
		
		this.data.push(claim);

		let claimObj = JSON.stringify({
			user_id: claim.author.id,
			text: claim.text,
			likes: 0,
			likes_id: ""
		});

		
		let token_param = '?token=' + this.token;

		// HTTP POST TO API
		this.http.post(ClaimService.SERVER + 'claims' + token_param, claimObj).subscribe((response) => {
			
			callback(response);
		}, error => {
			callback(error);
		})
	}

	getUsersLikes(id) {
		let token_params 	= '?token=' + this.token;
		let action 			= 'claims/' + id + '/users';
		
		return this.http.get(ClaimService.SERVER + action + token_params);
	}


	/**
	 * Execute like process
	 * @param {Object} item Claim Object
	 */
	like(item) {
		// Només fer like si no ho hem fet abans
		if (!item.liked) {
			
			let token_param = '?token=' + this.token; 
			
			item.likes++;
			item.liked = true;
			
			// HTTP POST save LIKE
			this.http.get(ClaimService.SERVER + 'claims/' + item.id + '/like' + token_param).subscribe(response => {
				
				if(response.json().error != false) {
					// Set liked
					item.liked = false;
					item.likes--;
				}
			}, error => {
				console.log(error);
			})
		}
	}
}