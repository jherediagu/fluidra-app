import {Page, NavController, NavParams, Translate, TranslatePipe} from 'ionic-angular';
import {ClaimCardComponent} from '../../components/claim';
import {UserItemComponent} from '../../components/user_item';
import {Claim} from '../../models/Claim';
import {User} from '../../models/User';
import {ClaimService} from '../../services/claims_service';
import {UserProfile} from '../userprofile/userprofile';
import {LocaleFactory}  from '../../factories/LocaleFactory';
@Page({
    templateUrl: "build/pages/likes/likes.html",
    providers: [ClaimService],
    pipes: [TranslatePipe],
    directives: [ClaimCardComponent, UserItemComponent]
})
export class LikesPage {

    claim: Claim;
    users: Array<User> = [];
    lang: string;

    constructor(private nav: NavController, private params: NavParams, private service: ClaimService, private i18n: Translate){
        
        this.initLocales();

        this.claim = params.get('claim');
        if(this.claim.id > 0) this.getUsers();
        
    }

    /**
     * Init the Locales i18n object
     */
    private initLocales() {
        // Idiomes
        this.lang = LocaleFactory.getInstance().getCurrentLang();
        this.i18n.setLanguage(this.lang);
        this.i18n.translations(this.lang, LocaleFactory.getInstance().getLocales());
    }


    viewProfile(user) {
        this.nav.push(UserProfile, {
			user: user
		});
    }


    getUsers(){
        console.log('get users likes...');
        this.service.getUsersLikes(this.claim.id).subscribe((response) => {
            if(response.json().error == false) {
                let data = response.json().data;
                data.forEach((u) => {
                    let user = new User(u.id, u.name, u.surname, u.company, u.email, u.country, true, u.picture);
                    this.users.push(user);
                });
                console.log('LIKES');
                console.log(this.users); 
            }
        }, (error) => {
            console.log('ERROR');
        })
    }

}