import {NavController, NavParams, Page, Alert, Loading, Translate, TranslatePipe} from 'ionic-angular';
import {Bases} 			from '../bases/bases';
import {AvisLegal} 		from '../avislegal/avislegal';
import {ClaimsPage} 	from '../claims/claims';
import {Ranking} 		from '../ranking/ranking';
import {Claim} 			from '../../models/Claim';
import {User} 			from '../../models/User';
import {UserFactory} 	from '../../factories/UserFactory';
import {ClaimService} 	from '../../services/claims_service';
import {LocaleFactory}  from '../../factories/LocaleFactory';

@Page({
  templateUrl: 'build/pages/participa/page1.html',
  providers: [ClaimService],
  pipes: [TranslatePipe]
})
export class Participa {
	
	claimServ: ClaimService;
	claim: Claim;
	nav: NavController;
	i18n: Translate;
    lang: string;

	constructor(nav: NavController, claimService: ClaimService, i18n: Translate) {

		// Inicialice services
		this.claimServ 	= claimService;
		this.nav 		= nav;


		// CurrentUser
		let user = UserFactory.getInstance().getCurrentUser();
		
		// Inicialice claim object with User Information
		this.createClaim();
		
		this.i18n    = i18n;
        this.initLocales();
	}
	
	/**
     * Init the Locales i18n object
     */
    private initLocales() {
        // Idiomes
        this.lang = LocaleFactory.getInstance().getCurrentLang();
        this.i18n.setLanguage(this.lang);
        this.i18n.translations(this.lang, LocaleFactory.getInstance().getLocales());
    }	


	private createClaim() {
		// CurrentUser
		let user = UserFactory.getInstance().getCurrentUser();
		
		// Inicialice claim object with User Information
		this.claim = new Claim();
		this.claim.likes = 0;
		this.claim.liked = false;
		this.claim.created_at = new Date().toDateString();
		// Author
		this.claim.setAuthor(user);
	}


	private showLoading() {
		let l = Loading.create({
			content: this.i18n.translate("Publicando...", this.lang)
		});
		
		this.nav.present(l);
		
		return l;
	}


	/**
	 * addClaim()
	 * Adds a new claim to Database
	 */
	addClaim(){
		
		if(this.claim.text.trim().length > 0) {
			// Show loading spinner	
			let loading = this.showLoading();
			
			// JSON.stringify, avoid TypeError: Converting circular structure to JSON
			// http://stackoverflow.com/questions/11616630/json-stringify-avoid-typeerror-converting-circular-structure-to-json
			this.claim.author.claims = [];
			
			// Transform object into JSON and stringify 
			var claim_to_save = JSON.parse(JSON.stringify(this.claim));
			
			// Use service to send to API
			this.claimServ.add(claim_to_save, (response) => {
				
				// Complete! Hide loading spinner
				loading.dismiss();
				
				// Set the new ID for this claim
				this.claim.id = response.json().data.id;
				
				// Show a new present
				if (response.json().error == false) this.presentSaveOk();
				else {
					this.presentSaveKO(response.json().error);
				}
				
				
				// Reset textarea
				this.createClaim();
			});
		}
	}



	showBases(){
		this.nav.push(Bases);
	}

	showLegal(){
		this.nav.push(AvisLegal);
	}

	showClaimsPage() {
		this.nav.push(ClaimsPage);
	}
	
	showTop5Page() {
		this.nav.push(Ranking);
	}
	

	presentSaveKO(msg?) {

		if(msg == "default_error_message") {
			msg = this.i18n.translate("No se ha podido publicar. Intentelo más tarde.", this.lang);
		}

		let alert = Alert.create({
			title: this.i18n.translate("Atención", this.lang),
			subTitle: msg,
			buttons: [
				{
					text: this.i18n.translate("Aceptar", this.lang)
				}
			]
		});

		this.nav.present(alert);
	}

	presentSaveOk() {
		let alert = Alert.create({
			title: this.i18n.translate("¡Publicado!", this.lang),
			subTitle: this.i18n.translate("Se ha publicado tu propuesta, muchas gracias por participar.", this.lang),
			buttons: [
				{
					text: this.i18n.translate("Aceptar", this.lang), handler: data => {
						
						this.returnBack();
					}
				}
			]
		});

		this.nav.present(alert);
	}
	
	
	
	private returnBack() {
		console.log('Return back');
		setTimeout(() => {
			
			let first = this.nav.indexOf(this.nav.first());
			let last = this.nav.indexOf(this.nav.last());
			
			this.nav.push(ClaimsPage).then(res => this.nav.remove(first + 1, last - first));
			
		}, 500);
	}
}
