import {Page, NavController, Alert, ActionSheet, Loading, Translate, TranslatePipe, Storage, LocalStorage} from 'ionic-angular';
import { FORM_DIRECTIVES, FormBuilder,  ControlGroup, Validators, AbstractControl } from 'angular2/common';


import {Camera, ImagePicker, Device} from 'ionic-native'; 
import {User} 					 	 from '../../models/User';
import {UserFactory} 				 from '../../factories/UserFactory';
import {UserService}				 from '../../services/users_service';
import {DashPage}					 from '../dash/dash';
import {LoginPage} 					 from '../login/login';
import {LocaleFactory}  			 from '../../factories/LocaleFactory';
import {TokenFactory}  				 from '../../factories/TokenFactory';


@Page({
	templateUrl: 'build/pages/profile/profile.html',
	providers: [UserService],
	pipes: [TranslatePipe],
	directives: [FORM_DIRECTIVES]
})
export class Profile {

	currentUser: User;
	nav: NavController;
	userService: UserService;
	localS: Storage;
	i18n: Translate;
    lang: string;
	
	/*
	FORMS
	*/
	profileForm: ControlGroup;
	name: AbstractControl;
	surname: AbstractControl;
	email: AbstractControl;
	company: AbstractControl;
	
		

	constructor(nav: NavController, userService: UserService, i18n: Translate, fb: FormBuilder) {
		
		this.nav 			= nav;
		this.userService 	= userService;
		this.localS 		= new Storage(LocalStorage);
		
		this.initForm(fb);
		
		// Get current User or create new
		this.currentUser = UserFactory.getInstance().getCurrentUser();
		if (this.currentUser == null) {
			this.currentUser = new User();
		}
		
		this.i18n    = i18n;
        this.initLocales();			
	}
	
	
	private initForm(fb: FormBuilder) {
		this.profileForm 	= fb.group({
			'name': ['', Validators.compose([Validators.required])],
			'surname': ['', Validators.compose([Validators.required])],
			'email': ['', Validators.compose([Validators.required, Validators.pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}')])],
			'company': ['', Validators.compose([Validators.required])]
		});
		
		
		// Inputs 
		this.name 		= this.profileForm.controls['name'];
		this.surname 	= this.profileForm.controls['surname'];
		this.email 		= this.profileForm.controls['email'];
		this.company 	= this.profileForm.controls['company'];
	}
	
	
	/**
     * Init the Locales i18n object
     */
    private initLocales() {
        // Idiomes
        this.lang = LocaleFactory.getInstance().getCurrentLang();
        this.i18n.setLanguage(this.lang);
        this.i18n.translations(this.lang, LocaleFactory.getInstance().getLocales());
    }
	
	
	/**
	 * Reload all locales
	 */
	private refreshLocales() {
		this.initLocales();
	}	


	
	/**
	 * Show ActionSheet with Pictures options 
	 */
	takePicture(){
		this.presentActionSheet();
	}



	/**
	 * Save this info to API.
	 * ======================
	 * If the user object is null, register a new user and send to DashPage
	 * If the user not empty, update user and show DashPage
	 */
	save() {
		
		console.log(this.profileForm);
		console.log(this.profileForm.valid);
		
		if(this.profileForm.valid) {		
			// Show Loading spinner
			let loading = this.showLoading();
			
			// Send to API
			this.userService.save(this.currentUser).subscribe((results) => {
				
				// Save on factory
				if(this.currentUser)
				UserFactory.getInstance().setCurrentUser(this.currentUser);
				
				// Hide Loading spinner
				loading.dismiss();

				// Save language on LocaleFactory
				LocaleFactory.getInstance().setCurrentLang(this.currentUser.language);
				this.lang = this.currentUser.language;
				this.refreshLocales();
				
				let info = results.json(); 
				
				if(info.error != "false" && info.error !== false) this.presentSaveError("Error: " + info.error);
				else {
					this.presentSaveOk();
					// Save into app user info.
					if(this.currentUser.id == 0 || typeof this.currentUser.id == "undefined") {
						console.log('Current User', this.currentUser);
						console.log('Data', info.data);
						if(this.currentUser.id == null) {
							this.currentUser.id = info.data.id;
							// Set new token
							TokenFactory.getInstance().setToken(info.data.private_code);
							this.localS.set("login_user", JSON.stringify({email: info.data.email, pass: this.currentUser.password}));
							this.localS.set('token', info.data.private_code);
						}
					}
				} 
			
			 

			} , error => {
				// Show error message
				this.presentSaveError(this.i18n.translate("No hemos podido guardado las modificaciones.", this.lang));	
			});
		}
		else this.presentSaveError("Campos incorrectos");
	}


	/**
	 * showLoading
	 * ==
	 * Show Loading Spinner
	 * 
	 * @return Loading Object
	 */
	private showLoading() {
		let l = Loading.create({
			content: this.i18n.translate("Guardando...", this.lang)
		});
		
		this.nav.present(l);
		
		return l;
	}
	
	/**
	 * From Camera
	 * ===========
	 * Get Picture from Camera and save it on user's object
	 */
	uploadImageFromCamera(){
		/*
		### Camera.DestinationType 
		| Name       | Type   | Default | Description |
		| ----       | ----   | ------- | ----------- |
		| DATA_URL   | number | 0       | Return base64 encoded string. DATA_URL can be very memory intensive and cause app crashes or out of memory errors. Use FILE_URI or NATIVE_URI if possible |
		| FILE_URI   | number | 1       | Return file uri (content://media/external/images/media/2 for Android) |
		| NATIVE_URI | number | 2       | Return native uri (eg. asset-library://... for iOS) |
		 */
		var options = { 
			quality: 100,
			destinationType: Camera.DestinationType.DATA_URL,
			saveToPhotoAlbum: true,
			allowEdit: true,
			cameraDirection: Camera.Direction.FRONT,
			targetWidth: 150,
			targetHeight: 150
		};
		
		// Get Pic!
		Camera.getPicture(options).then((imgData) => {
			this.currentUser.picture = "data:image/jpeg;base64," + imgData;
		}, (err) => {
			this.presentSaveError(this.i18n.translate("No hemos podido usar la imagen.", this.lang));
		});
	}
	
	/**
	 * Remove all data and send it to LoginPage
	 */
	logout() {
		
		// Reset all data
		UserFactory.getInstance().setCurrentUser(new User());
		let localS = new Storage(LocalStorage);
      	localS.remove('login_user').then(res => {  
			this.nav.setRoot(LoginPage);
		});
	}
	
	/**
	 * From Gallery
	 * ============
	 * Get 1 image from gallery and save it on user's object.
	 */
	uploadImageFromGallery() {
		var options = {
			quality: 90,
			destinationType: Camera.DestinationType.DATA_URL,
			sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
			allowEdit: true,
			encodingType: Camera.EncodingType.JPEG,
			targetWidth: 100,
			targetHeight: 100,
			saveToPhotoAlbum: false
		};
		Camera.getPicture(options).then((imgData) => {
			this.currentUser.picture =  "data:image/jpeg;base64," +  imgData;
		});
	}
	
	
	/**
	 * Show OK present
	 */
	presentSaveOk(){
		
		var nav = this.nav;
		this.nav.present(Alert.create({
			title: this.i18n.translate("¡Guardado!", LocaleFactory.getInstance().getCurrentLang()),
			subTitle: this.i18n.translate("Se han guardado las modificaciones.", LocaleFactory.getInstance().getCurrentLang()),
			buttons: [{
				text: this.i18n.translate("Aceptar", LocaleFactory.getInstance().getCurrentLang()),
				handler: () => {
					// Go to DashPage!
					nav.setRoot(DashPage);
				}
			}]	
		}));
	}

	/**
	 * Show KO present
	 */
	presentSaveError(error: string) {
		var nav = this.nav;
		let lang = LocaleFactory.getInstance().getCurrentLang();
		this.nav.present(Alert.create({
			title: this.i18n.translate("¡Error!", lang),
			subTitle: error,
			buttons: [{
				text: this.i18n.translate("Aceptar", lang),
				handler: () => {
					// Go to DashPage!
					//nav.setRoot(DashPage);
					nav.pop();
				} 
			}]
		}));
	}


	/**
	 * ActionSheet
	 * ===========
	 * Show ActionSheet with options
	 */
	presentActionSheet() {
		// Detect platform
		let platform = Device.device.platform;
		// ActionSheet buttons
		let btns = new Array();
		btns = [
			{
				icon: 'image',
				text: this.i18n.translate('Escoger de la galería', this.lang),
				role: 'gallery',
				handler: () => {
					this.uploadImageFromGallery();
				}
			}, {
				icon: 'camera',
				text: this.i18n.translate('Camara de fotos', this.lang),
				handler: () => {
					this.uploadImageFromCamera();
				}
			}, {
				text: this.i18n.translate('Cancelar', this.lang),
				role: 'cancel',
				handler: () => {
					// Nothing
				}
			}
		];
		
		// if device is iOS not include icons.
		if(platform == "iOS") {
			btns[0].icon = "";
			btns[1].icon = "";
			btns[2].icon = "";
		}
		
		let actionSheet = ActionSheet.create({
			title: this.i18n.translate('Añadir foto desde...', this.lang),
			buttons: btns
		});
		
		// Show ActionSheet
		this.nav.present(actionSheet);
	}

}