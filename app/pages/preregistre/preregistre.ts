import {Page, NavController, Translate, TranslatePipe} from 'ionic-angular';
import {Profile} from '../profile/profile';
import {LocaleFactory}  from '../../factories/LocaleFactory';

@Page({
	templateUrl: "build/pages/preregistre/preregistre.html",
	pipes: [TranslatePipe]
})
export class PreRegistrePage {
	
	accepted: boolean = false;
	nav: NavController;
	i18n: Translate;
    lang: string;	

	constructor(nav: NavController, i18n: Translate) {
		this.nav = nav;
		
		this.i18n    = i18n;
        this.initLocales();				
	}
	
	/**
     * Init the Locales i18n object
     */
    private initLocales() {
        // Idiomes
        this.lang = LocaleFactory.getInstance().getCurrentLang();
        this.i18n.setLanguage(this.lang);
        this.i18n.translations(this.lang, LocaleFactory.getInstance().getLocales());
    }		

	/**
	 * Accepts all use conditions
	 */
	accept() {
		this.accepted = true;
		// Show Profile Edit page
		this.nav.push(Profile);
	}
}