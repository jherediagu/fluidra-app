import {BarcodeScanner, Splashscreen} from 'ionic-native';
import {Page, NavController, Storage, LocalStorage, Alert, Translate, TranslatePipe} from 'ionic-angular';
import {LoginPage} from '../login/login';
import {DashPage} from '../dash/dash';
import {AuthService} from '../../services/auth_service';
import {LocaleFactory}  from '../../factories/LocaleFactory';


@Page({
	templateUrl: 'build/pages/preauth/preauth.html',
	providers: [AuthService],
	pipes: [TranslatePipe]
})
export class PreAuthPage {
	
	nav: NavController;
	code: string;
	localS: Storage; 
	authService: AuthService;
	i18n: Translate;
    lang: string;	

	constructor(nav: NavController, authService: AuthService, i18n: Translate) {
		this.nav = nav;
		this.authService = authService;
		
		// This user is previously authorized?
		this.localS = new Storage(LocalStorage);
      	this.localS.get('auth_code').then((code) => {  
			// if authorized redirects to LoginPage
			if(code === "true") this.nav.setRoot(LoginPage);
			else Splashscreen.hide();  
		});
		
		this.i18n    = i18n;
        this.initLocales();		
	}
	
	/**
     * Init the Locales i18n object
     */
    private initLocales() {
        // Idiomes
        this.lang = LocaleFactory.getInstance().getCurrentLang();
        this.i18n.setLanguage(this.lang);
        this.i18n.translations(this.lang, LocaleFactory.getInstance().getLocales());
    }	



	/**
	 * Scan QR code with native component
	 */
	scanCode() {
		BarcodeScanner.scan().then((barcodeData) => {
			// Success! Barcode data is here
			this.code = barcodeData.text;
			// Check this code on API
			this.checkCode();
			
		}, (err) => {
			// An error occurred
			this.nav.present(Alert.create({
				title: "Error",
				message: this.i18n.translate("No se ha podido escanear correctamente el código.", this.lang)
			}));
			// Reset code
			this.code = '';
		});
	}



	/**
	 * Check code are correct.
	 * 
	 */
	checkCode() {
		
		
	
		// Use AuthService to send this code to API and wait a response 
		this.authService.preAuth(this.code).subscribe((response) => {
			console.log('PreAuth: ', response.json());
			
			if(response.json().authorized === true) {
				this.localS.clear();
				this.localS.set('auth_code', "true");
				
				// Go to Login Page
				this.nav.push(LoginPage);
			}
			else {
				this.nav.present(Alert.create({
					title: "Error",
					message: this.i18n.translate("Este código no es correcto", this.lang),
					buttons: ["Aceptar"]
				}));
				
				// Reset code
				this.code = '';
			}
			
		}, error => {
			this.nav.present(Alert.create({
				title: "Error",
				message: "Server error",
				buttons: ["Aceptar"]
			}));
		});
	}
}