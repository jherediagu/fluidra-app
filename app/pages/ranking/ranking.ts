import {Page, Translate, TranslatePipe, NavController} from 'ionic-angular';
import {ClaimService}	from '../../services/claims_service';
import {ReversePipe} 	from '../../pipes/reverse';
import {LocaleFactory}  from '../../factories/LocaleFactory';
import {Claim} from '../../models/Claim';
import {Participa}  from '../participa/page1';
import {UserProfile} from '../userprofile/userprofile';
import {ClaimCardComponent} from '../../components/claim';
import {OrderBy}		from '../../pipes/orderBy';
import {LikesPage} from '../likes/likes';
import {SearchPage} from '../search/search';

@Page({
  templateUrl: 'build/pages/ranking/ranking.html',
  providers: [ClaimService],
  pipes: [ReversePipe, TranslatePipe, OrderBy],
  directives: [ClaimCardComponent]
})
export class Ranking {

	nav: NavController;
	top: Array<Claim> = [];
	service: ClaimService;
	i18n: Translate;
    lang: string;

	constructor(data: ClaimService, i18n: Translate, nav: NavController) {
		this.service = data;
		this.nav = nav;

		this.service.getTopClaims().subscribe(response => {
			
			if(response.json().data != null && response.json().data.length > 0) {
				
				var items = response.json().data;
				items.forEach(item => {
					this.top.push(this.service.transformToClaim(item));
				});
			}
			
		}, error => {
			console.log('Error', "Can't have top 5 :^(");
		});
		
		
		// i18n
		this.i18n    = i18n;
        this.initLocales();
	}
	
	/**
     * Init the Locales i18n object
     */
    private initLocales() {
        // Idiomes
        this.lang = LocaleFactory.getInstance().getCurrentLang();
        this.i18n.setLanguage(this.lang);
        this.i18n.translations(this.lang, LocaleFactory.getInstance().getLocales());
    }
	
	/**
	 * viewAuthorProfile
	 * ==
	 * Show author proile
	 */
	viewAuthorProfile(u) {
		console.log('Send user:', u);
		this.nav.push(UserProfile, {
			user: u
		});
	}


	showLikeList(claim) {
		this.nav.push(LikesPage, {
			claim: claim
		});
	}		


	searchPage(){
		this.nav.push(SearchPage);
	}

	tapLike($event, item) {
		if($event.tapCount == 2) {
			this.like(item)
		}
	}

	like(item) {
		this.service.like(item);
	}
	
	
	
	addClaimPage() {
		this.nav.push(Participa);
	}
}
