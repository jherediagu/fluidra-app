import {Page, Translate, TranslatePipe} from 'ionic-angular';
import {LocaleFactory}  from '../../factories/LocaleFactory';

@Page({
	templateUrl: 'build/pages/bases/bases.html',
	pipes: [TranslatePipe]
})
export class Bases {
	
    i18n: Translate;
    lang: string;
	
	constructor(i18n: Translate) {
        this.i18n    = i18n;
        this.initLocales();
	}
	
    /**
     * Init the Locales i18n object
     */
    private initLocales() {
        // Idiomes
        this.lang = LocaleFactory.getInstance().getCurrentLang();
        this.i18n.setLanguage(this.lang);
        this.i18n.translations(this.lang, LocaleFactory.getInstance().getLocales());
        //this.i18n.translate("Me gusta mucho el jamon.", this.lang));
    }
}
