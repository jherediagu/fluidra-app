import {Page, NavController, NavParams, Alert, Loading} from 'ionic-angular';

import {User} from '../../models/User';
import {Claim} from '../../models/Claim';
import {UserService} from '../../services/users_service';
import {ClaimService} from '../../services/claims_service';
import {ClaimCardComponent} from '../../components/claim';
import {OrderBy} from '../../pipes/orderBy';
import {LikesPage} from '../likes/likes';
@Page({
    templateUrl: 'build/pages/userprofile/userprofile.html',
    providers: [UserService, ClaimService],
    pipes: [OrderBy],
    directives: [ClaimCardComponent]
})
export class UserProfile {
    
    user: User;
    Claims: ClaimService;
    Users: UserService;
    nav: NavController;
    
    constructor(service: UserService, claims_service: ClaimService, nav: NavController, navParams: NavParams) {
        this.Users  = service;
        this.nav    = nav;
        this.Claims = claims_service;
        
        // Get the user form navigation params
        this.user = navParams.get('user');
        
        console.log('Recived User:', this.user);
        
        // Get claims from service
        this.initUserClaims();
    }
    
    
    private initUserClaims() {
        
        // delete all current instance claims
        this.user.claims = [];
        
        // Get from server
        this.Claims.getUserClaims(this.user.id).subscribe(response => {
           
           if(response.json().data != null && response.json().data.length > 0) {
				
				var items = response.json().data;
				items.forEach(item => {
					this.user.claims.push(this.Claims.transformToClaim(item));
				});
			}
            console.log('Update USER', this.user);
        }, error => {
            this.user.setClaimsFromArray([]);
        });
    }



    /**
     * showLikeList
     * ==
     * show user list liked this claim
     */
    showLikeList(claim) {
		this.nav.push(LikesPage, {
			claim: claim
		});
	}
    
    
    /**
	 * Like to claim
	 * ==
	 * @param {Claim} item
	 */
	like(claim) {
		this.Claims.like(claim);
	}
    
    
}