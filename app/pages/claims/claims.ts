import {Page, NavController, Translate, TranslatePipe, Content} from 'ionic-angular';
import {ViewChild} from 'angular2/core';
import {UserProfile} from '../userprofile/userprofile';
import {ClaimService} from '../../services/claims_service';
import {Claim} 	from '../../models/Claim';
import {OrderBy} from '../../pipes/orderBy';
import {LocaleFactory} from '../../factories/LocaleFactory';
import {Participa} from '../participa/page1';
import {UserFactory} from '../../factories/UserFactory';
import {User} from '../../models/User';
import {ClaimCardComponent} from '../../components/claim';
import {LikesPage} from '../likes/likes';
import {SearchPage} from '../search/search';

@Page({
	templateUrl: "build/pages/claims/claims.html",
	providers: [ClaimService],
	pipes: [OrderBy, TranslatePipe],
	directives: [ClaimCardComponent]
})
export class ClaimsPage {
	@ViewChild(Content) content: Content;
	
	public static PAGINATION: number = 10;

	nav: NavController;
	service: ClaimService;
	page: number = 0;
	lang: string;
	i18n: Translate;
	// Claims 
	claims: Array<Claim> 	= [];
	top: Array<Claim> 		= [];
	ownclaims: Array<Claim> = [];
	last_id: number = 0;
	
	
	// Default section from segment...
	section: string = "Eslóganes";
	
	constructor(nav: NavController, data: ClaimService, i18n: Translate) {
		this.nav = nav;
		this.service = data;
		
		// Returns last 5 claims
		this.initAllClaims();
		
		// Returns ranking claims
		this.initTopClaims();
		
		// Retrieve current User claims
		this.initOwnClaims();
		
		
        this.i18n   = i18n;
        this.initLocales();
	}
	
	/**
	 * Retrieve all last claims
	 */
	private initAllClaims() {
		this.service.retrieveData(ClaimsPage.PAGINATION);
		this.claims = this.service.getData();
	}
	
	
	/**
	 * Retrieve top5 claims
	 */
	private initTopClaims() {
		this.service.getTopClaims().subscribe(response => {
			
			if(response.json().data != null && response.json().data.length > 0) {
				
				var items = response.json().data;
				items.forEach(item => {
					this.top.push(this.service.transformToClaim(item));
				});
			}
			
		}, error => {
			console.log('Error', "Can't have top 5 :^(");
		});
	}
	
	
	private initOwnClaims() {
		this.service.getUserClaims().subscribe(response => {
			if(response.json().data != null && response.json().data.length > 0) {
				
				var items = response.json().data;
				items.forEach(item => {
					this.ownclaims.push(this.service.transformToClaim(item));
				});
			}
		});
	}
	
	
	
	
	
    /**
     * Init the Locales i18n object
     */
    private initLocales() {
        // Idiomes
        this.lang = LocaleFactory.getInstance().getCurrentLang();
        this.i18n.setLanguage(this.lang);
        this.i18n.translations(this.lang, LocaleFactory.getInstance().getLocales());
    }
	
	/**
	 * Scroll content to top
	 */
	scrollToTop() {
		this.content.scrollToTop();
	}
	
	searchPage(){
		this.nav.push(SearchPage);
	}
	
	/**
	 * Push to Participa
	 */
	addClaimPage() {
		this.nav.push(Participa);
	}
	
	/**
	 * Like to claim
	 * ==
	 * @param {Claim} item
	 */
	like(item) {
		this.service.like(item);
	}


	showLikeList(claim) {
		this.nav.push(LikesPage, {
			claim: claim
		});
	}
	
	
	/**
	 * viewAuthor
	 * ==
	 * Show author profile
	 */
	viewAuthor(u) {
		console.log('view author page:', u);
		
		this.nav.push(UserProfile, {
			user: u
		});
	}
	
	
	/**
	 * doRefresh
	 * ===
	 * Check new claims from server
	 * If have, this claims push in the top of list.
	 */
	doRefresh(refresher) {
		if(this.service.http_response) {
			
			// 
			// first_id because the list is orderby id DESC 
			//                                         ====
			
			this.service.newerThan(this.service.http_response.first_id, ClaimsPage.PAGINATION);
			this.claims = this.service.getData();	
		}
		setTimeout(() => {
			refresher.complete();
		}, 1000);
	}



	/**
	 * doInfinite
	 * ==
	 * Retrieve old claims from server
	 */
	doInfinite(infiniteScroll) {
		console.log('Begin async operation');
		this.page++;
		setTimeout(() => {
			
			// get last claim (most older)

			let last = this.claims[this.claims.length - 1];
			
			this.service.olderThan(last.id, 10);
			this.claims = this.service.getData();
			console.log('Async operation has ended');
			infiniteScroll.complete();
		}, 500);
	}
}