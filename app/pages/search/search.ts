import {Page, NavController, NavParams, Translate, TranslatePipe} from 'ionic-angular';
import {UserItemComponent} from '../../components/user_item';
import {User} from '../../models/User';
import {UserService} from '../../services/users_service';
import {UserProfile} from '../userprofile/userprofile';
import {LocaleFactory}  from '../../factories/LocaleFactory';
@Page({
    templateUrl: "build/pages/search/search.html",
    providers: [UserService],
    pipes: [TranslatePipe],
    directives: [UserItemComponent]
})
export class SearchPage {

    users: Array<User> = [];
    items: Array<User> = [];
    lang: string;
    searchQuery: string;

    constructor(private nav: NavController, private service: UserService, private params: NavParams, private i18n: Translate){
        this.initLocales();
        this.initializeItems();
        this.searchQuery = "";        
    }

    /**
     * Init the Locales i18n object
     */
    private initLocales() {
        // Idiomes
        this.lang = LocaleFactory.getInstance().getCurrentLang();
        this.i18n.setLanguage(this.lang);
        this.i18n.translations(this.lang, LocaleFactory.getInstance().getLocales());
    }


    viewProfile(user) {
        this.nav.push(UserProfile, {
			user: user
		});
    }


    getItems(searchbar) {
        
        // Reset items back to all of the items
        this.initializeItems();

        // set q to the value of the searchbar
        var q = searchbar.value;

        // if the value is an empty string don't filter the items
        if (q.trim() == '') {
            return;
        }

        this.users = this.items.filter((v) => {
            if (v.fullName().toLowerCase().indexOf(q.toLowerCase()) > -1) {
                return true;
            }

            return false;
        });
    }


    resetItems() {
        this.users = this.items;
    }

    initializeItems(){
        
        if(this.items.length == 0) {
            this.service.all().subscribe((response) => {
                if(response.json().error == false) {
                    let data = response.json().data;
                    data.forEach((u) => {
                        let user = new User(u.id, u.name, u.surname, u.company, u.email, u.country, true, u.picture);
                        this.items.push(user); // permanent
                        this.users.push(user); // volatil
                    });      
                }
            }, (error) => {
                console.log('ERROR');
            });
        }
        else this.resetItems();
    }

}