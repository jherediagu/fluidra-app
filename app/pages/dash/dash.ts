import {Page, NavController, NavParams, Translate, TranslatePipe, Alert} from 'ionic-angular';
import {Splashscreen} from 'ionic-native';

import {Participa}  from '../participa/page1';
import {Profile}    from '../profile/profile';
import {Ranking}    from '../ranking/ranking';
import {Bases}      from '../bases/bases';
import {AvisLegal}  from '../avislegal/avislegal';
import {ClaimsPage}     from '../claims/claims';
import {UserFactory}    from '../../factories/UserFactory';
import {User}           from '../../models/User';
import {LocaleFactory}  from '../../factories/LocaleFactory';


@Page({
    templateUrl: 'build/pages/dash/dash.html',
    pipes: [TranslatePipe]
})
export class DashPage {
    
    nav: NavController;

    i18n: Translate;
    lang: string;


    constructor(nav: NavController, i18n: Translate) {
    	this.nav = nav;
        this.i18n    = i18n;
        
        // init Locales
        this.initLocales();	
        
        // Hide SplashScreen
		Splashscreen.hide();
        
   	}
       
    
    onPageWillEnter() {
        
        console.log('onPageWillEnter');
        this.initLocales();	
       
	}

    /**
     * Init the Locales i18n object
     */
    private initLocales() {
         // Idiomes
		if(UserFactory.getInstance().getCurrentUser() != null)
			LocaleFactory.getInstance().setCurrentLang(UserFactory.getInstance().getCurrentUser().language);
        
        // Idiomes
        this.lang = LocaleFactory.getInstance().getCurrentLang();
        this.i18n.setLanguage(LocaleFactory.getInstance().getCurrentLang());
        this.i18n.translations(LocaleFactory.getInstance().getCurrentLang(), LocaleFactory.getInstance().getLocales());
    }

    participar() {
    	this.nav.push(Participa);
    }
    perfil() {
        this.nav.push(Profile);
    }
    ranking() {
        this.nav.push(Ranking);
    }
    claims() {
        this.nav.push(ClaimsPage);
    }
    bases() {
        this.nav.push(Bases);
    }
    aviso_legal() {
        this.nav.push(AvisLegal);
    }
}