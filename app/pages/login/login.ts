import {Page, NavController, Alert, Storage, LocalStorage, Loading, Translate, TranslatePipe} from 'ionic-angular';
import {Splashscreen} 		from 'ionic-native';
import {DashPage} 			from '../dash/dash/';
import {Profile} 			from '../profile/profile';
import {PreRegistrePage}	from '../preregistre/preregistre';
import {User} 				from '../../models/User';
import {UserFactory} 		from '../../factories/UserFactory';
import {AuthService} 		from '../../services/auth_service';
import {UserService}		from '../../services/users_service';
import {LocaleFactory}  	from '../../factories/LocaleFactory';
import {TokenFactory} 		from '../../factories/TokenFactory';


@Page({
    templateUrl: 'build/pages/login/login.html',
    providers: [AuthService, UserService],
	pipes: [TranslatePipe]
})
export class LoginPage {
	
	
	user;
	authS: AuthService;
	nav: NavController;
	localS: Storage;
	userS: UserService;
	i18n: Translate;
    lang: string;

	constructor(nav: NavController, auth: AuthService, userService: UserService, i18n: Translate) {
		this.nav 	= nav;
		this.authS 	= auth;
		this.userS 	= userService;
		this.localS = new Storage(LocalStorage);
		this.i18n    = i18n;
		
		// We have user instance?
		if(UserFactory.getInstance().getCurrentUser()) {
			this.nav.setRoot(DashPage);
		}
		else {
			// Check if have user login into localStorage
			this.checkUserLocalStorage();
			
			// Init user
			this.initUser();
		}
		
		this.initLocales();
	}
	
	/**
     * Init the Locales i18n object
     */
    private initLocales() {
        // Idiomes
		if(UserFactory.getInstance().getCurrentUser() != null)
			LocaleFactory.getInstance().setCurrentLang(UserFactory.getInstance().getCurrentUser().language);
		
		
        this.lang = LocaleFactory.getInstance().getCurrentLang();
        this.i18n.setLanguage(this.lang);
        this.i18n.translations(this.lang, LocaleFactory.getInstance().getLocales());
    }
	
	
	/**
	 * Check have a basic info about this user and re-login
	 */
	checkUserLocalStorage() {
		this.localS.get('login_user').then((user) => {
			if(user != null) {
				user = JSON.parse(user);
				if(user.email != null && user.pass != null) {
					this.login(user.email, user.pass);	
				}
			}
			else Splashscreen.hide();
		});
	}
	
	

	/**
	 * Init User object used by form
	 */
	initUser() {
		this.user = { email: "", password: "" };
	}

	/**
	 * Reset User object used in form
	 */
	resetUser() {
		this.initUser();
	}


	/**
	 * Execute process to Recovery password
	 * @param {string} email [description]
	 */
	recoverPassword(email: string) {
		this.userS.recoverPassword(email).subscribe(response => {
			
			this.nav.pop();
			let title = "";
			let message = this.i18n.translate("Te hemos enviado un correo con instrucciones para restablecer tu contraseña", this.lang);
			
			if(response.json().error != false) {
				title = "Error";
				message = this.i18n.translate("El sistema no ha podido restablecer la contraseña", this.lang);
			}
			
			
			this.nav.present(new Alert({
				title: title,
				message: message,
				buttons: [{text: this.i18n.translate("Aceptar", this.lang), handler: data => {
					this.nav.pop();
				}}]
			}));
			
		});
	}


	/**
	 * Presents a modal with little form to recovery password.
	 * User can enter own email to send it to API
	 */
	presentRecoveryPassword() {
		let alert = Alert.create({
			title: this.i18n.translate("Recuperación de contraseña", this.lang),
			subTitle: this.i18n.translate("Escriba el correo electrónico con el que se dió de alta y le enviaremos un enlace para resetear su contraseña.", this.lang),
			inputs: [
				{ name: 'email', placeholder: this.i18n.translate('Correo electrónico', this.lang), type: 'email' }
			],
			buttons: [
				{ text: this.i18n.translate("Cancelar", this.lang), role: "Cancel" },
				{ 
					text: this.i18n.translate("Aceptar", this.lang), handler: data => {
						this.recoverPassword(data.email);
					}
				}
			]
		});

		this.nav.present(alert);
	}


	/**
	 * Presents a Modal with error information
	 */
	presentErrorLogin(error?) {

		let alert = Alert.create({
			title: this.i18n.translate("Atención", this.lang),
			subTitle: this.i18n.translate("Email o contraseña incorrectas.", this.lang),
			message: error,
			buttons: [this.i18n.translate("Aceptar", this.lang)]
		});
		
		

		// Show this alert
		this.nav.present(alert);
	}


	/**
	 * Show login prompt
	 */
	presentLogin() {
		let alert = Alert.create({
			title: this.i18n.translate("Acceder", this.lang),
			subTitle: this.i18n.translate("Escriba su correo electrónico y contraseña.", this.lang),
			inputs: [
				{ name: 'email', placeholder: this.i18n.translate('Correo electrónico', this.lang), type: 'email' },
				{ name: 'password', placeholder: this.i18n.translate('Contraseña', this.lang), type: 'password' }
			],
			buttons: [
				{ text: this.i18n.translate("Cancelar", this.lang), role: "Cancel" },
				{
					text: this.i18n.translate("Aceptar", this.lang), handler: data => {
						this.login(data.email, data.password);
					}
				}
			]
		});

		this.nav.present(alert);
	}


	showLogin() {
		this.presentLogin();
	}


	showSignIn() {
		this.nav.push(PreRegistrePage);
	}

	/**
	 * Login process
	 * @param {string} email [description]
	 * @param {string} pass  [description]
	 */
	login(email: string, pass: string) {
		
		
		
		if(email && pass) {
			this.user.email 	= email;
			this.user.password 	= pass;
		}
		
		
		this.authS.auth(email, pass).subscribe(response => {
			console.log(response.json());
			if(response.json().authorized) {
				
				// Extract all APIs response data
				let u = response.json().data;
				
				// Create User with all data from API.
				let currentUser = new User(u.id, u.name, u.surname, u.company, u.email, u.country, u.accept_legal, u.picture, u.created_at, false, u.language);
				if(u.claims !== null) currentUser.setClaimsFromArray(u.claims);
				
				// Save on User Factory Singleton
				UserFactory.getInstance().setCurrentUser(currentUser);	
				
				// Save this info into LocalStorage again
				this.localS.set("login_user", JSON.stringify({email: this.user.email, pass: this.user.password}));
				this.localS.set("token", u.private_code);
				TokenFactory.getInstance().setToken(u.private_code);
				
				// Send to API this Device information
				this.userS.sendUserDeviceInfo().subscribe();
				
				// Got to dashPage
				this.nav.setRoot(DashPage);
			}
			else {
				
				// If login is incorrect reset token and login_user store
				this.localS.remove("login_user");
				this.localS.remove("token");
				
				// Hide SplashScreen 
				Splashscreen.hide();
				// This params are incorrect
				this.presentErrorLogin();
				// Reset inputs
				this.resetUser();
			}
		}, error => {
			// Have another error on Login process
			this.presentErrorLogin(JSON.stringify(error.json()));
			// Reset inputs
			this.resetUser();
		});
	}
}