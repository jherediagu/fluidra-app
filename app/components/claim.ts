import {IONIC_DIRECTIVES, Translate, TranslatePipe} from 'ionic-angular';
import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {Claim} from '../models/Claim';
import {LocaleFactory}  from '../factories/LocaleFactory';

@Component({
    selector: 'claim-card',
    template: `
        <ion-card>
            <ion-item tappable (click)="profile(claim.author)">
                <ion-avatar item-left>
                    <img *ngIf="!claim.author.picture" src="img/profile-default2.png">
                    <img *ngIf="claim.author.picture" src="{{claim.author.picture}}" >
                </ion-avatar>
                <h2>{{claim.author.fullName()}}</h2>
                <p>{{claim.author.company}}</p>
            </ion-item>
            <ion-card-content >
                <ion-card-title >
                    {{claim.text}}
                </ion-card-title>
            </ion-card-content>
            <ion-item *ngIf="options">
                <button (click)="doLike()" flu-green clear item-left>
                    <ion-icon *ngIf="!claim.liked" name="heart-outline"></ion-icon>
                    <ion-icon *ngIf="claim.liked" name="heart"></ion-icon>
                    <div>{{claim.likes}}</div>
                </button>
                <button (click)="who()" *ngIf="claim.likes > 0" flu-green clear>
                    <ion-icon ios="ios-pulse" md="md-pulse"></ion-icon>
                </button>
                <ion-note item-right>
                    {{claim.created_at}}
                </ion-note>
            </ion-item>
        </ion-card>
    `,
    pipes: [TranslatePipe],
    directives: [IONIC_DIRECTIVES]
})
export class ClaimCardComponent {
    @Input() claim: Claim;
    @Input() options: Boolean = true;
    @Output() openProfile: EventEmitter<any> = new EventEmitter();
    @Output() whoLiked: EventEmitter<any> = new EventEmitter();
    @Output() like: EventEmitter<any> = new EventEmitter();

    lang: string;

    constructor(private i18n: Translate){
        this.initLocales();
    }

    /**
     * Init the Locales i18n object
     */
    private initLocales() {
        // Idiomes
        this.lang = LocaleFactory.getInstance().getCurrentLang();
        this.i18n.setLanguage(this.lang);
        this.i18n.translations(this.lang, LocaleFactory.getInstance().getLocales());
    }

    profile(author) {
        this.openProfile.emit(author);
    }

    doLike() {
        this.like.emit(this.claim);
    }

    who() {
        this.whoLiked.emit(this.claim);
    }
}