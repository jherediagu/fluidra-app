import {IONIC_DIRECTIVES} from 'ionic-angular';
import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {User} from '../models/User';

@Component({
    selector: 'user-item',
    template: `
        <ion-item (click)="profile(user)" *ngIf="user">
            <ion-avatar item-left>
                <img *ngIf="!user.picture" src="img/profile-default2.png">
                <img *ngIf="user.picture" src="{{user.picture}}" >
            </ion-avatar>
            <h2>{{user.name}} {{user.surname}}</h2>
            <p>{{user.company}}</p>
        </ion-item>
    `,
    directives: [IONIC_DIRECTIVES]
})
export class UserItemComponent {
    
    @Input() user: User;
    @Output() openProfile: EventEmitter<any> = new EventEmitter();
    
    constructor(){}

    profile(user) {
        this.openProfile.emit(user);
    }

}