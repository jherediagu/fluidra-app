import {Claim} from './Claim';
/**
 * User Model
 * ==========
 * @author Mario M. <mario@javajan.com>
 */
export class User {
	
	id: number;
	name: string;
	surname: string;
	company: string;
	email: string;
	password: string;
	picture: string;
	country: string;
	language: string;
	accepted_legal: boolean;
	private_code: string;
	created_at: Date;
	first_time: boolean;
	// Special
	authorized: boolean;
	auth_token: string;
	// Own Claims
	claims: Array<Claim> = [];

	/**
	 * Inicialice a User Object
	 * @param {number}  id             Database ID
	 * @param {string}  name           Single name of user
	 * @param {string}  surname        Surname
	 * @param {string}  company        Company 
	 * @param {string}  email          Valid email 
	 * @param {string}  country        Country 
	 * @param {boolean} accepted_legal The user accepts legal conditions?
	 * @param {string}  picture        http route to get picture 
	 * @param {Date}    created_at     new Date
	 */
	constructor(id?: number, name?: string, surname?: string, company?: string, email?: string, country?: string, accepted_legal?: boolean, picture?: string, created_at?: Date, first_time?: boolean, language?: string) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.company = company;
		this.email = email;
		this.country = country;
		this.accepted_legal = accepted_legal;
		this.picture = picture;
		this.first_time = first_time;
		this.created_at = created_at;
		this.language = language;
	}

	/**
	 * fullName
	 * 
	 * Return a string composed by name + surname
	 * 
	 * @returns {string} Fullname 
	 */
	fullName() {
		return this.name + ' ' + this.surname;
	}

	/**
	 * Set the user's token used to communicate with API.
	 * @param {string} token Token provided by API SERVER for this user.
	 */
	setAuthToken(token: string) {
		this.auth_token = token;
		this.private_code = token;
	}

	/**
	 * Returns the user's token
	 * @returns {string} token
	 */
	getAuthToken() {
		return this.auth_token;
	}
	
	/**
	 * setClaimsFromArray
	 * --
	 * Iterate claim's array and push into Claims prop.
	 *   
	 * @param  {Array<Claim>} claims
	 */
	setClaimsFromArray(claims) {
		claims.forEach(c => {
			// Transform to boolean
			if(c.liked > 0 ) c.liked = true;
			else c.liked = false;
			
			let claim = new Claim(c.id, c.likes, c.liked, c.text, c.created_at);
			this.claims.push(claim);
		});
	}

	/**
	 * Return authorized value
	 * @returns {boolean} True or False
	 */
	isAuth(){
		return this.authorized;
	}
}