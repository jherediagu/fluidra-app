import {User} from './User';

export class Claim {

	id: number;
	likes: number;
	liked: boolean; // ve de la API??
	text: string;
	country: string;
	created_at: string;
	// Author of Claim
	author: User;

	/**
	 * Claim Object
	 * @param {number}  id         [description]
	 * @param {string}  name       [description]
	 * @param {number}  likes      [description]
	 * @param {boolean} liked      [description]
	 * @param {string}  company    [description]
	 * @param {string}  text       [description]
	 * @param {string}    created_at [description]
	 */
	constructor(id?: number, likes?: number, liked?: boolean, text?: string, created_at?: string) {
		this.id = id;
		this.likes = likes;
		this.liked = liked;
		this.text = text;
		this.created_at = created_at;
	}

	setAuthor(u: User) {
		this.author = u;
	}

	getAuthor() {
		return this.author;
	}
}