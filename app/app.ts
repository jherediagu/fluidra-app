import {App, Platform} from 'ionic-angular';
import {StatusBar, Splashscreen, Globalization} from 'ionic-native';
import {PreAuthPage} from './pages/preauth/preauth';
import {LocaleFactory} from './factories/LocaleFactory';



@App({
  template: '<ion-nav [root]="rootPage"></ion-nav>',
  config: {
    backButtonText: '',
  } // http://ionicframework.com/docs/v2/api/config/Config/
})
export class MyApp {
  rootPage: any = PreAuthPage;

  constructor(platform: Platform) {
    platform.ready().then(() => {
      
      this.hideSplashScreen();

      Globalization.getPreferredLanguage().then(result => {
        let lang = result.value.substring(0,2);
        if(!LocaleFactory.getInstance().isSuported(lang)) {
            lang = 'es';
        }
        
        LocaleFactory.getInstance().setCurrentLang(lang);  
      });
      
      
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });
  }


  hideSplashScreen() {
    if(Splashscreen) { 
      setTimeout(()=> { Splashscreen.hide(); }, 100); 
    } 
  }


}
