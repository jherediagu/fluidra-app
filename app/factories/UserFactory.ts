import {User} from '../models/User';

export class UserFactory {
	
	static instance: UserFactory;
	static isCreating: boolean = false;

	currentUser: User;
	
	constructor() {
		if (!UserFactory.isCreating) {
			throw new Error("You can't call new in Singleton instances!");
		}
	}

	static getInstance() {
		if(UserFactory.instance == null) {
			UserFactory.isCreating = true;
			UserFactory.instance = new UserFactory();
			UserFactory.isCreating = false;
		}

		return UserFactory.instance;
	}

	setCurrentUser(user: User) {
		this.currentUser = user;
	}

	getCurrentUser() {
		return this.currentUser;
	}
}