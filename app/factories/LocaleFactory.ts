export class LocaleFactory {

	locales: Object = {
		'es': {

			'Usuarios': 'Usuarios',

			'usuarios': 'usuarios',

			'encontrados': 'encontrados',

			'Buscar': 'Buscar',
			

			'A quien le gusta?': 'A quien le gusta?',

			'Actividad': 'Actividad',

			'CÓDIGO' : 'CÓDIGO',
			
			'Míos': 'Míos',
			
			//Avis legal
			"_TITLE1": "AVISO LEGAL Y CONDICIONES DE USO DE LA APP",

			"_AVIS1": "FLUIDRA S.A, (en adelante \"FLUIDRA\"), con domicilio social en Avda. Francesc Macià 60, pl. 20 de Sabadell (Barcelona), inscrita en el Registro Mercantil de Barcelona, en el tomo 36883, folio 132, hoja núm. B 290316, provista de CIF A-17728593, es titular de la APP \"Our New Slogan\" (En adelante “la App”).",

			"_TITLE2": "ACCESO Y CONDICIONES DE USO DE LA APP",

			"_AVIS2": "Las presentes Condiciones de Uso (en adelante “las Condiciones”), tienen por finalidad explicar al usuario de la APP (en adelante “el Usuario”) el funcionamiento de la misma.",

			"_AVIS2.1": "El acceso y utilización de la APP supone la aceptación por el Usuario, sin reservas de ninguna clase, de todas y cada una de las presentes Condiciones. Si no acepta las presentes condiciones, le rogamos se abstenga de utilizar la APP.",

			"_AVIS2.2": "FLUIDRA podrá, en todo momento y sin previo aviso, modificar las presentes Condiciones, todo ello expuesto en la propia APP, mediante la publicación de dichas modificaciones en la misma, con el fin de que puedan ser conocidas por los Usuarios, siempre antes de comenzar el uso de la APP.",

			"_AVIS2.3": "FLUIDRA podrá denegar el acceso a la APP a los Usuarios que hagan un mal uso de los contenidos y/o incumplan cualquiera de las condiciones que aparecen en el presente documento.",

			"_SUBTITOL1": "Acceso y Utilización",

			"_AVIS3": "El acceso a la APP es gratuito, salvo en lo relativo al coste de la conexión a través de la red de telecomunicaciones suministrada por el proveedor de acceso contratado por el Usuario.",

			"_AVIS3.1": "El usuario se compromete a hacer un uso diligente de la APP, así como de la información contenida en la misma, con total sujeción tanto a la normativa aplicable, como a las presentes Condiciones.",

			"_SUBTITOL2": "Obligación de hacer un uso correcto",

			"_AVIS4": "Los Usuarios son íntegramente responsables de su conducta al acceder a la información de la APP, mientras hagan uso de la misma, así como después de haber accedido.",

			"_AVIS4.1": "El Usuario se compromete a la correcta utilización de la APP y utilidades que se le proporcionen conforme a la Ley, las presentes Condiciones, las instrucciones y avisos que se le comuniquen, así como con la moral y las buenas costumbres generalmente aceptadas y al orden público.",

			"_AVIS4.2": "El Usuario se obliga al uso exclusivo de la APP, así como todos sus contenidos, para fines lícitos y no prohibidos, que no infrinjan la legalidad vigente y/o puedan resultar lesivos de los derechos legítimos del GRUPO FLUIDRA o de cualquier tercero y/o que puedan causar cualquier daño o perjuicio de forma directa o indirecta.",

			"_AVIS4.3": "De conformidad con lo dispuesto anteriormente, se entenderá por contenido sin que esta enumeración tenga carácter limitativo: los textos, fotografías, gráficos, imágenes, iconos, tecnología, software, links y demás contenidos audiovisuales o sonoros, así como su diseño gráfico y códigos fuente (en adelante, todo ello el/los \"Contenido/s\"), de conformidad con la Ley, las presentes Condiciones, los demás avisos, reglamentos de uso e instrucciones puestos en su conocimiento, así como con la moral y las buenas costumbres generalmente aceptadas y el orden público, y, en particular, se compromete a abstenerse de: reproducir, copiar, distribuir, poner a disposición o de cualquier otra forma comunicar públicamente, transformar o modificar los Contenidos, a menos que se cuente con la autorización del titular de los correspondientes derechos o ello resulte legalmente permitido.",

			"_AVIS4.5": "El Usuario se compromete, a título meramente enunciativo y no limitativo, a no transmitir, difundir o poner a disposición de terceros informaciones, datos, contenidos, mensajes, gráficos, dibujos, archivos de sonido y/o 1imagen, fotografías, grabaciones, software y, en general, cualquier clase de material propiedad de esta APP, así como a abstenerse de realizar actos, que:",

			"_AVIS4.6": "- Induzcan, inciten o promuevan actuaciones delictivas, difamatorias, violentas o, en general, contrarias a la Ley, a la moral y buenas costumbres generalmente aceptadas o al orden público;",

			"_AVIS4.7": "- Sean falsos, ambiguos, inexactos, exagerados o extemporáneos, de forma que induzcan o puedan inducir a error sobre su objeto o sobre las intenciones o propósitos del comunicante;",

			"_AVIS4.8": "- Se encuentren protegidos por cualesquiera derechos de propiedad intelectual o industrial pertenecientes a terceros, sin que el Usuario haya obtenido previamente de sus titulares la autorización necesaria para llevar a cabo el uso que efectúa o pretende efectuar;",

			"_AVIS4.9": "- Constituyan, en su caso, publicidad ilícita, engañosa o desleal y, en general, que constituyan competencia desleal o vulneren las normas reguladoras de la protección de datos de carácter personal.",

			"_AVIS4.10": "- Incorporen virus u otros elementos físicos o electrónicos que puedan dañar o impedir el normal funcionamiento de la red, del sistema o de equipos informáticos (hardware y software) de FLUIDRA.",

			"_AVIS4.11": "- Provoquen por sus características (tales como formato o extensión, entre otras) dificultades en el normal funcionamiento de los servicios ofrecidos por esta APP.",

			"_AVIS4.12": "El usuario responderá de los daños y perjuicios de toda naturaleza que FLUIDRA pueda sufrir como consecuencia del incumplimiento de cualesquiera de las obligaciones a las que queda sometido en virtud de las presentes Condiciones o de la legislación aplicable en relación con la utilización de la APP.",

			"_SUBTITOL3": "EXONERACIÓN DE RESPONSABILIDAD DE FLUIDRA",

			"_AVIS5": "A título enunciativo pero no limitativo, FLUIDRA no asumirá responsabilidad alguna:",

			"_AVIS5.1": "- De la utilización que los Usuarios puedan hacer de los materiales contenidos en esta APP, ya sean prohibidos o permitidos, en infracción de los derechos de propiedad intelectual y/o industrial de contenidos de la APP o de terceros.",

			"_AVIS5.2": "- De los eventuales daños y perjuicios a los Usuarios causados por un funcionamiento normal o anormal de las herramientas de búsqueda, de la organización o la localización de los contenidos y/o acceso a la APP y, en general, de los errores o problemas que se generen en el desarrollo o instrumentación de los elementos técnicos que la APP o un programa facilite al Usuario.",

			"_SUBTITOL4": "FLUIDRA informa que no garantiza:",

			"_AVIS6": "- Que el acceso a la APP y/o a las webs de enlace sea ininterrumpido o de error.",

			"_AVIS6.1": "- Que el contenido o software al que los Usuarios accedan a través de la APP o de las webs de enlace no contenga error alguno, virus informático u otros elementos en los contenidos que puedan producir alteraciones en su sistema o en los documentos electrónicos y ficheros almacenados en su sistema informático o cause otro tipo de daño;",

			"_AVIS6.2": "La información contenida en la APP debe ser considerada por los Usuarios como divulgativa y orientadora, tanto con relación a su finalidad como a sus efectos, motivo por el cual FLUIDRA no garantiza la exactitud de dicha información y por consiguiente no asumen responsabilidad alguna sobre los posibles perjuicios o incomodidades para los Usuarios que pudiesen derivarse alguna inexactitud presente en el mismo.",

			"_SUBTITOL5": "POLÍTICA DE PRIVACIDAD: PROTECCIÓN DE DATOS PERSONALES",

			"_AVIS7": "Los Usuarios se comprometen a acceder a la APP y a utilizar el contenido de la misma de buena fe.",

			"_AVIS7.1": "En cumplimiento de lo establecido en la Ley Orgánica 15/1999, de Protección de Datos de Carácter Personal, informamos que, los datos de carácter personal incorporados en la APP:",

			"_AVIS7.2": "1o.- Serán tratados con la máxima confidencialidad y formarán parte de los ficheros titularidad de FLUIDRA S.A para la gestión de la APP, así como para la gestión de la relación laboral que los vincule.",

			"_AVIS7.3": "2o.- Implica la aceptación de esta política de privacidad, así como la autorización a FLUIDRA para que trate los datos personales que se le faciliten.",

			"_AVIS7.4": "Conforme a la legislación vigente en materia de protección de datos, FLUIDRA ha adoptado los niveles de seguridad adecuados a los datos facilitados por los Usuarios y, además, ha instalado todos los medios y medidas a su alcance para evitar la pérdida, mal uso, alteración, acceso no autorizado y extracción de los mismos.",

			"_SUBTITOL6": "Derechos de acceso, rectificación, cancelación y oposición",

			"_AVIS8": "En cualquier momento y conforme establece la citada Ley Orgánica, los Usuarios podrán ejercitar los derechos de acceso, rectificación, cancelación y oposición de sus datos personales, mediante petición escrita dirigida a la dirección de correo electrónico lopd@fluidra.com o a la dirección postal: Avda. Francesc Macià 60, pl. 20 de Sabadell, (Barcelona), dirigida al Departamento de Recursos Humanos indicando, según sea conveniente, de forma visible, el concreto derecho que se ejerce, así como adjuntando copia del DNI.",

			"_SUBTITOL7": "GUÍA DE USO SEGURO:",

			"_SUBTITOL8": "Phishing",

			"_AVIS9": "No utilice enlaces incorporados en e-mails o páginas Web de terceros para acceder a esta APP. Periódicamente se detectan envíos de correos masivos indiscriminados, remitidos desde direcciones electrónicas falsas, con el único objetivo de conseguir información confidencial de los Usuarios. A esta técnica se la conoce con el nombre de \"PHISHING\". FLUIDRA declina cualquier responsabilidad ajena a este respecto.",

			"_SUBTITOL9": "LEGISLACIÓN APLICABLE Y JURISDICCIÓN",

			"_AVIS10": "El acceso y utilización de la APP se regirá e interpretará de conformidad con la legislación española. Cualquier controversia que pudiera surgir entre FLUIDRA y los usuarios de la APP, será dirimida, con renuncia expresa de las Partes a su propio fuero, por los Juzgados y Tribunales de la ciudad de Barcelona.",
			//End Avis legal


			//Bases
			"_TITLE_BASES": "BASES DEL CONCURSO \"Our New Slogan\" ",

			"_BASES1": "Objetivo: En línea con la nueva Misión de Fluidra “Cuidar del bienestar y de la salud de las personas a través del uso sostenible del agua en sus aplicaciones lúdicas, deportivas y terapéuticas”, el objetivo de este concurso es encontrar entre todos los integrantes de Fluidra, un nuevo slogan que refleje mejor el sentido más cercano y humano de la Misión, para substituir el que había hasta ahora “Making Water Perform”.",

			"_BASES2": "Tema: El concurso consistirá en la presentación de propuestas de un nuevo slogan para el GRUPO FLUIDRA por medio de una APP disponible en los siguientes idiomas: español, inglés, francés, alemán e italiano.",

			"_BASES3": "Se entiende un eslogan (claim, lema) como una frase clave o expresión que identifique el valor distintivo de la empresa. Tiene la capacidad de transmitir un poderoso mensaje capaz de reflejar la esencia de la compañía.",

			"_BASES4": "Participantes: Podrán participar en este concurso exclusivamente los empleados de todas las empresas del GRUPO FLUIDRA, con dos excepciones: los integrantes del Departamento de Comunicación Corporativa, así como los miembros del Jurado.",

			"_BASES5": "Inscripción: La inscripción es gratuita y se realiza mediante la creación de un perfil de usuario en la APP del concurso.",

			"_BASES6": "Admisión de propuestas, plazos y requisitos: No habrá límite de número de propuestas por cada participante. Los idiomas admitidos son español, inglés, francés, alemán e italiano.",

			"_BASES7": "Se admitirá la publicación de propuestas hasta el 30 de septiembre de 2016. Paralelamente, los participantes podrán darle “me gusta” a cuantas propuestas de otros participantes consideren.",

			"_BASES8": "El jurado valorará las propuestas a partir del 1 de octubre de 2016 y durante el mes de octubre se publicará el slogan ganador.",

			"_BASES9": "Jurado: El Jurado estará formado por el Comité Corporativo.",

			"_BASES10": "Criterios de elección de los proyectos ganadores: Aunque se tendrán en cuenta los “me gusta” de los participantes, estos no serán vinculantes y el Jurado valorará, para elegir el eslogan ganador:",

			"_BASES10.1": "- Aquellas propuestas más originales y creativas, que expresen los valores corporativos así como con la nueva misión de Fluidra: Cuidar del bienestar y de la salud de las personas a través del uso sostenible del agua en sus aplicaciones lúdicas, deportivas y terapéuticas.",

			"_BASES10.2": "- La propuesta ganadora deberá tener significado y sonar bien en inglés.",

			"_BASES11": "El concurso podría quedar desierto en caso de que el Jurado no encuentre ninguna propuesta adecuada para representar el eslogan del GRUPO FLUIDRA. El GRUPO FLUIDRA se reserva el derecho a realizar cualquier cambio y/o modificación o adaptación de la propuesta ganadora.",

			"_BASES12": "Premio: La publicación de la propuesta ganadora se realizará en la APP, el site livingfluidra.fluidra.com y vía correo electrónico al ganador.",

			"_BASES13": "El premio es un vale para un fin de semana en un hotel de wellness para 2 personas, valorado en 1.000 euros.",

			"_BASES14": "Entrega del premio por parte de un alto directivo de Fluidra y además se le realizará una entrevista en la revista online de Fluidra “LivingFluidra”.",

			"_BASES15": "El premio no se podrá canjear por dinero en efectivo.",

			"_BASES16": "Aceptación y descalificación: La descarga de la APP del concurso y la consiguiente creación de un perfil en el mismo, implica expresamente la aceptación de todos y cada uno de los puntos indicados en estas bases, y la aceptación del fallo inapelable del Jurado. El incumplimiento de estas bases dará lugar a la descalificación de la persona participante.",

			"_BASES17": "Protección de datos personales: A efectos de lo dispuesto en la normativa vigente relativa a la protección de datos de carácter personal, la compañía FLUIDRA S.A. con domicilio en Av. Francesc Macià, 60, planta 20, 08208 Sabadell, Barcelona, perteneciente al GRUPO FLUIDRA, le recuerda que sus datos personales han sido incorporados a un fichero creado y bajo la responsabilidad de FLUIDRA S.A., con el fin del desarrollo de la actividad de la Compañía.",

			"_BASES18": "Asimismo, le recordamos que sus datos personales, podrán ser objeto de cesión a las distintas compañías del GRUPO FLUIDRA que Ud. podrá consultar en cualquier momento a través de la Intranet de la compañía, para la misma finalidad y que dicha cesión es necesaria para el desarrollo, cumplimiento y control de su relación laboral con la compañía. Si Vd. no está de acuerdo con lo anterior, puede ejercitar en cualquier momento sus derechos de acceso, rectificación, cancelación y/o oposición mediante correo electrónico dirigido a lopd@fluidra.com.",

			"_BASES19": "Material presentado: El participante reconoce que el slogan presentadoen el concurso es original, de su propia creación y que no implica copia o plagio alguno.",

			"_BASES20": "El Participante cede íntegramente a favor de FLUIDRA S.A. cualquier derecho de propiedad intelectual y/ industrial que pueda derivar de los slogans propuestos, resulten o no ganadores. La cesión a la que se refiere este documento es en exclusiva, y su ámbito territorial es mundial. La duración de la citada cesión comprende el máximo dispuesto en la legislación internacional aplicable.",

			"_BASES21": "La presente cesión en exclusiva abarca, asimismo, todos los derechos de explotación inherentes a cualquier adaptación o modificaciones que por cualquier razón se efectúen en los slogans. FLUIDRA S.A. gozará de total libertad para la explotación de los derechos objeto de la presente cesión, pudiendo llevarla a cabo por sí misma o a través de terceros, a los que podrá conceder licencias totales o parciales, por el tiempo y en las condiciones que estime oportunas.",
			//End Bases

			//claims
			"Cargando más claims...": "Cargando más claims...",
			//End claims	

			//dash
			"Participar": "Participar",

			"Mi perfil": "Mi perfil",

			"Eslóganes": "Eslóganes",

			"Top 5": "Top 5",

			"Bases del concurso": "Bases del concurso",

			"Aviso legal": "Aviso legal",
			//End dash

			//firsttime
			"¡Bienvenido a Fluidra Slogans!": "¡Bienvenido a Fluidra Slogans!",

			"Queremos explicarte un poco de que va esta app.": "Queremos explicarte un poco de que va esta app.",

			"Participa": "Participa",

			"Desde esta app podrás participar aportando tus ideas para nuestro nuevo slogan. Los mejores se llevaran un buen premio.": "Desde esta app podrás participar aportando tus ideas para nuestro nuevo slogan. Los mejores se llevaran un buen premio.",

			"Valora": "Valora",

			"Valora las publicaciones de otros usuarios dandole al like.": "Valora las publicaciones de otros usuarios dandole al like.",

			"Disfruta": "Disfruta",

			"Sobretodo, disfruta de esta experiencia.": "Sobretodo, disfruta de esta experiencia.",

			"Continuar": "Continuar",
			//End firsttime

			//login
			"¡Buscamos un nuevo eslogan para Fluidra!": "¡Buscamos un nuevo eslogan para Fluidra!",

			"Participa en este emocionante concurso proponiendo eslóganes y valorando los de tus compañeros.": "Participa en este emocionante concurso proponiendo eslóganes y valorando los de tus compañeros.",

			"Entre todos podemos encontrar el mejor eslogan ¿Será el tuyo? Participa.": "Entre todos podemos encontrar el mejor eslogan ¿Será el tuyo? Participa.",

			"Registrarse": "Registrarse",

			"Ya estoy registrado": "Ya estoy registrado",

			"¿Olvidaste la contraseña?": "¿Olvidaste la contraseña?",

			"Recuperación de contraseña": "Recuperación de contraseña",

			"Escriba el correo electrónico con el que se dió de alta y le enviaremos un enlace para resetear su contraseña.": "Escriba el email con el que se dió de alta y le enviaremos un enlace para resetear su contraseña.",

			"Correo electrónico": "Correo electrónico",

			"Cancelar": "Cancelar",

			"Aceptar": "Aceptar",

			"Atención": "Atención",

			"Email o contraseña incorrectas.": "Email o contraseña incorrectas.",

			"Acceder": "Acceder",

			"Escriba su correo electrónico y contraseña.": "Escriba su email y contraseña.",

			"Contraseña": "Contraseña",
			//End login

			//participa
			"OK": "OK",

			"Escribe un nuevo eslogan...": "Escribe un nuevo eslogan...",

			"Publicando...": "Publicando...",

			"No se ha podido publicar. Intentelo más tarde.": "No se ha podido publicar. Intentelo más tarde.",

			"¡Publicado!": "¡Publicado!",

			"Se ha publicado tu propuesta, muchas gracias por participar.": "Se ha publicado tu propuesta, muchas gracias por participar.",
			//End participa

			//preauth
			"Para poder usar esta app debes introducir el código de autorización.": "Para poder usar esta app debes introducir el código de autorización.",

			"Escanear QR": "",

			"No se ha podido escanear correctamente el código.": "No se ha podido escanear correctamente el código.",

			"Este código no es correcto": "Este código no es correcto",
			//End preauth

			//preregistre
			"Acepto las condiciones": "Acepto las condiciones",

			"Condiciones y bases del concurso": "Condiciones y bases del concurso",
			//End preregistre

			//profile
			"Nombre": "Nombre",

			"Apellidos": "Apellidos",

			"Email": "Email",

			"Empresa": "Empresa",

			"Idioma": "Idioma",

			"Español": "Español",

			"Italiano": "Italiano",

			"Deutsch": "Deutsch",

			"English": "English",

			"Français": "Français",

			"País": "País",
			
			"Cerrar sesión" : "Cerrar sesión",

			"No hemos podido guardado las modificaciones.": "No hemos podido guardado las modificaciones.",

			"No hemos podido usar la imagen.": "No hemos podido usar la imagen.",

			"¡Guardado!": "¡Guardado!",

			"Se han guardado las modificaciones.": "Se han guardado las modificaciones.",

			"Escoger de la galería": "Escoger de la galería",

			"Camara de fotos": "Camara de fotos",

			"Añadir foto desde...": "Añadir foto desde...",
			//End profile

			//ranking
			// NO DATA
			//End ranking

			//tabs
			"Añadir": "Añadir"
			//End tabs
		},
		"en": {

			'Usuarios': 'Users',

			'usuarios': 'users',

			'encontrados': 'found',

			'Buscar': 'Search',

			'CÓDIGO' : 'AUTH CODE',

			'Actividad': 'Activity',
			
			'Míos': 'My claims',
			
			//Avis legal
			"_TITLE1": "LEGAL NOTICE AND APP USE CONDITIONS",

			"_AVIS1": "FLUIDRA S.A., (hereafter \"FLUIDRA\"), with business address at Avda. Francesc Macià 60, pl. 20, Sabadell (Barcelona province), registered with the Barcelona Commercial Register under volume 36883, folio 132, sheet no. B 290316, holder of Spanish Tax ID Code (CIF) A-17728593 is the owner of the \"Our New Slogan\" app (hereinafter“the APP”).",

			"_TITLE2": "ACCESS TO THE APP AND ITS USE CONDITIONS",

			"_AVIS2": "The present Use Conditions (hereinafter “the Conditions”) are designed to explain the use of the APP to the user (hereinafter “You”) and the way that it works.",

			"_AVIS2.1": "By accessing and using the APP, you accept, without reservations of any type, each and every one of the present Conditions. If you do not accept the present terms and conditions, please do not use the APP.",

			"_AVIS2.2": "FLUIDRA may, at any time and without previous notification, modify the present Conditions, all set forth in the APP itself, by publishing them on the APP to make them known to users, always before they begin to use it.",

			"_AVIS2.3": "FLUIDRA may deny access to the APP to users who make undue use of the content and/or who breach any of the terms and conditions of the present document.",

			"_SUBTITOL1": "Access and Use",

			"_AVIS3": "Access to the APP is free, except with regards the connection cost through the telecommunications network supplied by your access provider.",

			"_AVIS3.1": "You agree to make diligent use of the APP and the information contained in it, and to abide by the applicable regulations and the present Conditions.",

			"_SUBTITOL2": "Duty of correct use",

			"_AVIS4": "You are fully responsible for your behaviour when you access the information on the APP, while you make use of it, and after having accessed it.",

			"_AVIS4.1": "You undertake to use the APP and the utilities it provides correctly and in accordance with the law, the present Conditions, the instructions and notifications of which you are informed and with generally accepted moral standards, good customs and public order.",

			"_AVIS4.2": "You undertake to use the APP and all of its content for lawful and non-prohibited purposes that do not breach the prevailing laws and/or could harm the legitimate rights of the FLUIDRA GROUP or any third party and/or could cause direct or indirect damage or harm.",

			"_AVIS4.3": "In accordance with the above and by way of example but not limited to these, content shall be understood as: the texts, photographs, graphs, images, icons, technology, software, links and other audiovisual or sound content, as well as the graphic design and source codes (hereinafter, all of these shall be referred to as the “Content”), in accordance with the law, the present Conditions, other notifications, use regulations and instructions made available to you and with generally accepted moral standards, good customs and public order and, in particular, you undertake to abstain from: reproducing, copying, distributing, making available or in any other way publicly communicating, transforming or modifying the Content, unless you are authorised by the title holder of the corresponding rights or it is legally permitted.",

			"_AVIS4.5": "By way of example but not limited to these, you undertake to not transmit, disseminate or make available to third parties any information, data, content, messages, graphs, illustrations, sound and/or image files, photographs, 1recordings, software or, in general, any type of material that is the property of this APP, and shall also abstain from performing acts which:",

			"_AVIS4.6": "- Induce, incite or promote criminal, defamatory or violent actions or ones which, in general, are against the law, generally accepted moral standards, good customs or public order;",

			"_AVIS4.7": "- Are false, ambiguous, imprecise, exaggerated or extemporaneous, which induce or may induce error regarding their subject or the intentions or purposes of the party that communicates them;",

			"_AVIS4.8": "- Are protected by any intellectual or industrial property rights pertaining to third parties, without having previously obtained from their owners the authorisation needed for the use you make or wish to make of them;",

			"_AVIS4.9": "- Constitute, where applicable, unlawful, misleading or unfair advertising and, in general, onstitute unfair competition or violate personal data protection regulations.",

			"_AVIS4.10": "- Upload viruses or other physical or electronic elements that may damage or impede the normal operation of FLUIDRA’s network, system or IT equipment (hardware and software).",

			"_AVIS4.11": "- Cause, by their characteristics(such as format or extension, among others) difficulties in the normal operation of the services afforded by this APP.",

			"_AVIS4.12": "You shall respond for damage and harm of all types that FLUIDRA may suffer as a consequence of non- compliance with any of the obligations to which you are subject by irtue of the present Conditions or the applicable laws in relation to the use of the APP.",

			"_SUBTITOL3": "EXONERATION OF FLUIDRA’S LIABILITY",

			"_AVIS5": "By way of example but not limited to these, FLUIDRA shall not be held liable for:",

			"_AVIS5.1": "- Any use you make of the material contained in the APP, whether prohibited or permitted, in violation of the intellectual and/or industrial property rights of the content of the APP or third parties.",

			"_AVIS5.2": "- Any harm or damage caused to other users by the normal or abnormal operation of the search tools, organisation or localisation of the content and/or access to the APP and, in general, errors or problems generated in the development or instrumentation of the technical elements which the APP or a programme provides to you.",

			"_SUBTITOL4": "FLUIDRA informs you that it does not guarantee:",

			"_AVIS6": "- That access to the APP and/or linked websites will not be interrupted or contain errors.",

			"_AVIS6.1": "- That the content or software which you access through the APP or linked websites dos not contain any error, computer virus or other elements in the content that can produce changes to your system or the electronic documents and files stored on your computer system or cause any other type of damage.",
			"_AVIS6.2": "You must consider the information contained in the APP as informative and indicative, both in relation to purpose and effects, for which reason FLUIDRA does not guarantee the precision of said information and therefore assumes no responsibility for any possible damage or inconvenience to you which may derive from an imprecision present in the APP.",

			"_SUBTITOL5": "PRIVACY POLICY: PERSONAL DATA PROTECTION",

			"_AVIS7": "You undertake to access the APP and to use its content in good faith.",

			"_AVIS7.1": "In compliance with the provisions of Organic Law 15/1999 on Personal Data Protection, we hereby inform you that your personal data included in the APP:",

			"_AVIS7.2": "1.- Shall be processed with maximum confidentiality and form part of the files owned by FLUIDRA S.A. for managing the APP and the working relationship that governs them.",

			"_AVIS7.3": "2.- Means that you accept this privacy policy and authorise FLUIDRA to process the personal data you supply.",

			"_AVIS7.4": "In accordance with the prevailing legislation on data protection, FLUIDRA has adopted the appropriate security levels for the data you supply and has also installed all available means and measures to prevent the loss, undue use, alteration, non-authorised access and removal of them.",

			"_SUBTITOL6": "Rights of access, rectification, cancellation and opposition",

			"_AVIS8": "You may at any time and in accordance with the provisions of the abovementioned Organic Law exercise your rights of access, rectification, cancellation or opposition to the use of your personal data by writing to the email address lopd@fluidra.com or the postal address Avda. Francesc Macià 60, pl. 20, Sabadell, (Barcelona province), FAO the Human Resources Department, indicating the specific right you are exercising, where applicable, in a visible manner, and attaching proof of your identity (photocopy).",

			"_SUBTITOL7": "GUIDE TO SAFE USE:",

			"_SUBTITOL8": "Phishing",

			"_AVIS9": "Do not use links attached to emails or third-party websites to access this APP. Mass mailouts are regularly detected as having been sent from false email addresses with the sole purpose of obtaining confidential information on our users. This technique is known as \"PHISHING\". FLUIDRA declines all responsibility in this regard.",

			"_SUBTITOL9": "APPLICABLE LAWS AND JURISDICTION",

			"_AVIS10": "Access and use of the APP shall be governed and interpreted in accordance with Spanish laws. Any controversy that may arise between FLUIDRA and APP users shall be heard, with the express waiver by the parties to their own jurisdiction, by the Courts and Tribunals of the city of Barcelona.",
			//End avis legal


			//Bases
			"_TITLE_BASES": " \"Our New Slogan\" COMPETITION RULES",

			"_BASES1": "Purpose: In line with Fluidra’s new mission, “To take care of people’s well-being and health through the sustainable use of water for leisure, sports and therapeutic applications”, the purpose of this competition is to invite Fluidra employees to come up with a new slogan that better reflects the more approachable and people-oriented sense of the mission, to replace the slogan used to date, “Making Water Perform”.",

			"_BASES2": "Theme: The competition will consist of submitting proposals for a new slogan for the FLUIDRA GROUP via an APP available in the following languages: Spanish, English, French, German and Italian.",

			"_BASES3": "A slogan is understood to be a key phrase or expression that identifies the company’s distinctive value. It is able to convey a powerful message capable of reflecting the essence of the company.",

			"_BASES4": "Participants: The competition is open exclusively to employees of all FLUIDRA GROUP companies, with two exceptions: members of the Corporate Communications Department and members of the Jury.",

			"_BASES5": "Registration: It is free to register and you can enter the competition by creating a user profile on the competition APP.",

			"_BASES6": "Admission of proposals, deadlines and requisites: There is no limit on the number of proposals you can enter. The admitted languages are Spanish, English, French, German and Italian.",

			"_BASES7": "Proposals will be accepted until 30 September 2016. You can also “like” as many proposals from other participants as you want.",

			"_BASES8": "The jury will assess the proposals starting 1 October 2016 and the winning slogan will be published during the month of October.",

			"_BASES9": "Jury: The Jury will be formed by the Corporate Committee.",

			"_BASES10": "Election criteria for winning proposals: Although the participants’ “likes” will be considered, they will not be binding and the Jury will base its decision on the following criteria:",

			"_BASES10.1": "- The most original and creative proposals that express Fluidra’s corporate values and new mission of “To take care of people’s well-being and health through the sustainable use of water for leisure, sports and therapeutic applications”.",

			"_BASES10.2": "- The winning slogan must make sense and sound good in English.",

			"_BASES11": "The competition may be declared void if the Jury does not find any proposal suitable for representing the FLUIDRA GROUP slogan. FLUIDRA GROUP reserves the right to make any change and/or modification or adaptation to the winning entry.",

			"_BASES12": "Prize: The winning entry will be published on the APP and the livingfluidra.fluidra.com website and the winner will also be notified by email.",

			"_BASES13": "The prize is a voucher for a weekend for 2 in a wellness hotel valued at €1,000.",

			"_BASES14": "The prize will be awarded by a Fluidra senior executive and the winner will be interviewed for Fluidra’s online magazine “LivingFluidra”.",

			"_BASES15": "The prize cannot be redeemed in cash.",

			"_BASES16": "Acceptance and disqualification: By downloading the competition APP and creating a profile on it you expressly accept each and every one of the points indicated in these rules and the Jury’s final decision. Failure to comply with these rules will result in disqualification from the competition.",

			"_BASES17": "Personal data protection: In compliance with the provisions of the prevailing legislation on data protection, the company FLUIDRA S.A., with business address at Av. Francesc Macià, 60, planta 20, 08208, Sabadell, Barcelona province, part of the FLUIDRA GROUP, reminds you that your personal data will be included on a file created by and under the responsibility of FLUIDRA S.A., with the purpose of developing the Company’s activity.",

			"_BASES18": "We would furthermore remind you that your personal data may be assigned to the various companies of the FLUIDRA GROUP that you may consult at any time through the Company’s intranet for the same purpose, and that this assignment is necessary for the development, compliance and control of your employment relations with the Company. If you do not agree with the above, you may at any time exercise your rights of access, rectification, cancellation and/or opposition by emailing: lopd@fluidra.com.",

			"_BASES19": "Submitted material: By participating, you declare that the slogan you submit for the competition is original, is of your own creation and has not been copied or plagiarised.",

			"_BASES20": "You fully assign to FLUIDRA S.A. any intellectual and/or industrial property right that may arise from the proposed slogan, whether it wins or not. The assignment that this document refers to is exclusive and its territorial scope is worldwide. The duration of this assignment is the maximum time allowed for under applicable international laws. The present exclusive assignment also covers all exploitation rights inherent to any adaptation or modification made to the slogan for any reason.",

			"_BASES21": " FLUIDRA S.A. shall have complete freedom to exploit the rights that are the object of the present assignment and may practice them itself or through third parties, to whomfull or partial licences may be awarded for the time and under the conditions it considers appropriate.",
			//End Bases

			//claims
			"Cargando más claims...": "Loading more claims",
			//End claims	

			//dash
			"Participar": "Participate",

			"Mi perfil": "My profile",

			"Eslóganes": "Slogans",

			"Top 5": "Top 5",

			"Bases del concurso": "Competition rules",

			"Aviso legal": "Disclaimer",
			//End dash

			//firsttime
			"¡Bienvenido a Fluidra Slogans!": "Welcome to Fluidra Slogans!",

			"Queremos explicarte un poco de que va esta app.": "We want to tell you a bit about this app.",

			"Participa": "Participate",

			"Desde esta app podrás participar aportando tus ideas para nuestro nuevo slogan. Los mejores se llevaran un buen premio.": "You will be able to contribute your ideas for our new slogan from this app. The best will win a fantastic prize.",

			"Valora": "Rate",

			"Valora las publicaciones de otros usuarios dandole al like.": "Rate other users’ posts by clicking the ‘like’ button.",

			"Disfruta": "Enjoy",

			"Sobretodo, disfruta de esta experiencia.": " Above all, enjoy this experience.",

			"Continuar": "Next",
			//End firsttime

			//login
			"¡Buscamos un nuevo eslogan para Fluidra!": "We’re looking for a new slogan for Fluidra!",

			"Participa en este emocionante concurso proponiendo eslóganes y valorando los de tus compañeros.": "Participate in this exciting competition by submitting slogans and rating those of your work mates.",

			"Entre todos podemos encontrar el mejor eslogan ¿Será el tuyo? Participa.": "Between us, we will be able to come up with the best slogan. Will it be yours? Participate.",

			"Registrarse": "Sign up",

			"Ya estoy registrado": "I have already signed up",

			"¿Olvidaste la contraseña?": "Forgotten your password?",

			"Recuperación de contraseña": "Password recovery",

			"Escriba el correo electrónico con el que se dió de alta y le enviaremos un enlace para resetear su contraseña.": "Write the email used to sign up and we will send you a link to reset your password.",

			"Correo electrónico": "Email",

			"Cancelar": "Cancel",

			"Aceptar": "Accept",

			"Atención": "Attention",

			"Email o contraseña incorrectas.": "Incorrect email or password",

			"Acceder": "Enter",

			"Escriba su correo electrónico y contraseña.": "Write your email and password",

			"Contraseña": "Password",
			//End login

			//participa
			"OK": "Ok",

			"Escribe un nuevo eslogan...": "Write a new slogan…",


			"Publicando...": "publishing...",

			"No se ha podido publicar. Intentelo más tarde.": "It could not publish . Please try again later ",

			"¡Publicado!": "Publish!",

			"Se ha publicado tu propuesta, muchas gracias por participar.": "Your proposal has been published , thank you very much for participating.",
			//End participa

			//preauth
			"Para poder usar esta app debes introducir el código de autorización.": "To use this app you must enter your authorisation code.",

			"Escanear QR": "",

			"No se ha podido escanear correctamente el código.": "The code could not be properly scanned.",
			//End preauth

			//preregistre
			"Acepto las condiciones": "I accept the terms and conditions",

			"Condiciones y bases del concurso": "Terms and conditions, and competition rules",
			//End preregistre

			//profile
			"Nombre": "Name",

			"Apellidos": "Surname/s",

			"Email": "Email",

			"Empresa": "Company",

			"Idioma": "Language",

			"Español": "Español",

			"Italiano": "Italiano",

			"Deutsch": "Deutsch",

			"English": "English",

			"Français": "Français",

			"País": "Country",
			
			"Cerrar sesión" : "Logout",

			"No hemos podido guardado las modificaciones.": "We were unable to save the modifications.",

			"No hemos podido usar la imagen.": "We were unable to use the image.",

			"¡Guardado!": "Saved!",

			"Se han guardado las modificaciones.": "Your modifications have been saved.",

			"Escoger de la galería": "Select from gallery",

			"Camara de fotos": "Camera",

			"Añadir foto desde...": "Add photo from",
			//End profile

			//ranking
			// NO DATA
			//End ranking

			//tabs
			"Añadir": "Add"
			//End tabs
		},
		'it': {

			'Usuarios': 'Utenti',

			'usuarios': 'utenti',

			'encontrados': 'trovati',

			'Actividad': 'Attività',

			'Buscar': 'Cercare',


			'CÓDIGO' : 'CODICE',
			
			'Míos': 'Le mie slogan',
			
			//Avis legal
			"_TITLE1": "AVVERTENZA LEGALE E CONDIZIONI D'USO DELL'APP",

			"_AVIS1": "FLUIDRA S.A (d'ora innanzi, \"FLUIDRA\"), con sede in Avda. Francesc Macià 60, pl. 20, Sabadell (Barcelona),iscritta nel Registro delle Imprese di Barcellona al volume n. 36883, foglio n. 132, pagina n. B 290316 e avente Partita IVA n. A-17728593, è la titolare dell'APP \"Our New Slogan\" (d'ora innanzi, \"l'App\").",

			"_TITLE2": "ACCESSO E CONDIZIONI D'USO DELL'APP",

			"_AVIS2": "Lo scopo delle presenti condizioni d'uso (d'ora innanzi, \"le condizioni\") è quello di illustrare all'utente dell'APP (d'ora 			innanzi, l'\"Utente\") il funzionamento della medesima.",

			"_AVIS2.1": "Nel momento in cui l'utente accede all'APP e la utilizza questi accetta senza alcun tipo di riserva tutte le presenti			condizioni. Qualora non dovesse invece essere d'accordo con le presenti condizioni è cordialmente invitato a non utilizzare l'APP.",

			"_AVIS2.2": "In qualsiasi momento e senza preavviso FLUIDRA potrà modificare le presenti condizioni come esposto nell'APP stessa,ubblicandovi le modifiche in modo tale che gli utenti ne siano a conoscenza prima di iniziare a utilizzarla.",

			"_AVIS2.3": "FLUIDRA potrà negare l'accesso all'APP agli utenti che fanno un cattivo uso dei contenuti e/o non osservano qualche condizione del presente documento.",

			"_SUBTITOL1": "Accesso e uso",

			"_AVIS3": "L'accesso all'APP è gratuito, ad eccezione delle spese di connessione alla rete di telecomunicazione messa a disposizione dal fornitore di accesso scelto dall'utente.",

			"_AVIS3.1": "L'utente si impegna a utilizzare in maniera diligente l'APP, nonché le informazioni ivi contenute, attenendosi rigorosamente sia alla normativa applicabile che alle presenti condizioni.",

			"_SUBTITOL2": "Obbligo di uso corretto",

			"_AVIS4": "Gli utenti sono gli unici responsabili del proprio comportamento sia quando accedono alle informazioni dell'APP	durante l'uso della medesima che in seguito all'accesso.",

			"_AVIS4.1": "Pertanto, i medesimi si impegnano a utilizzare correttamente l'APP e le funzioni offerte conformemente alla legge, alle presenti condizioni, alle istruzioni e agli avvisi forniti, nonché alla morale, ai buoni usi e costumi di norma	accettati e all'ordine pubblico.",

			"_AVIS4.2": "L'utente si impegna altresì a utilizzare in maniera esclusiva l'APP e i suoi contenuti a fini leciti, non vietati né illegali, che non possano ledere i diritti legittimi del GRUPPO FLUIDRA o di qualsiasi terzo e/o che non possano causare direttamente né indirettamente danni di ogni genere.",

			"_AVIS4.3": "Conformemente a quanto anteriormente disposto, si considererà \"contenuto\" quanto citato di seguito a titolo	esemplificativo seppur non esaustivo: testi, fotografie, grafici, immagini, icone, tecnologia, software, link e altri materiali audiovisivi e sonori, compresi disegno grafico e codici sorgente (d'ora innanzi, denominati nel loro complesso \"contenuto/i\"), conformemente alla legge, alle presenti condizioni, ai restanti avvisi, regolamenti d'uso e istruzioni di cui l'utente è a conoscenza, nonché la morale, i buoni usi e costumi di norma accettati e l'ordine pubblico. Nello specifico, l'utente si impegna a non riprodurre, copiare, distribuire, mettere a disposizione o divulgare pubblicamente tramite qualsiasi mezzo, trasformare né modificare i contenuti, tranne nel caso in cui disponga dell'autorizzazione del titolare dei corrispondenti diritti, o nel caso in cui le suddette pratiche siano legalmente permesse.",

			"_AVIS4.5": "In particolare, e a titolo meramente indicativo e non esaustivo, l'utente si impegna a non trasmettere, diffondere né			mettere a disposizione di terzi informazioni, dati, contenuti, messaggi, grafici, disegni, file di suono e/ o immagini, fotografie, egistrazioni, software e, in generale, qualsiasi tipo di materiale appartenente a questa APP.Inoltre, rinuncia a compiere azioni che",

			"_AVIS4.6": "-	inducano, incitino o promuovano atti delittuosi, diffamatori, violenti o, in generale, contrari alla legge, alla 			morale e al buon senso generalmente accettato o all’ordine pubblico;",

			"_AVIS4.7": "-siano false, ambigue, inesatte, esagerate o estemporanee, inducendo o potendo indurre a errori in merito	all'oggetto, alle intenzioni o ai propositi di colui che le comunica;",

			"_AVIS4.8": "-siano protette da qualsiasi diritto di proprietà intellettuale o industriale appartenente a terzi, senza che l'Utente			abbia previamente ottenuto dai rispettivi titolari l'autorizzazione necessaria per realizzarne l'uso che effettua, o che			intende effettuare;",

			"_AVIS4.9": "-costituiscano, nel caso, pubblicità illecita, ingannevole o sleale e, in generale, rappresentino concorrenza sleale o infrangano le norme che regolano la protezione dei dati di carattere personale;",

			"_AVIS4.10": "-includano virus o altri elementi fisici o elettronici che possano danneggiare o impedire il normale	funzionamento della rete, del sistema o degli equipaggiamenti informatici (hardware e software) di FLUIDRA;",

			"_AVIS4.11": "-abbiano delle caratteristiche tali (ad es.formato o estensione) da provocare difficoltà nel normale	funzionamento dei servizi offerti da questa APP.",

			"_AVIS4.12": "L'utente risponderà dei danni di qualsiasi natura cagionati a FLUIDRA per via dell'inadempimento di qualsiasi			obbligo che gli spetta in virtù delle presenti condizioni o della legislazione applicabile in merito all'utilizzo dell'APP.",

			"_SUBTITOL3": "ESONERO DALLA RESPONSABILITÀ DI FLUIDRA",

			"_AVIS5": "A titolo esemplificativo ma non esaustivo, FLUIDRA non assumerà alcuna responsabilità in merito a:",

			"_AVIS5.1": "-Uso che gli utenti possono fare dei materiali contenuti nella presente APP, siano essi vietati o consentiti,infrangendo i diritti di proprietà intellettuale e/o industriale dei contenuti dell'APP o di terzi.",

			"_AVIS5.2": "-	Eventuali danni causati agli utenti per via di un funzionamento normale o anomalo degli strumenti di ricerca,dell'organizzazione o dell'ubicazione dei contenuti e/ o dell'accesso all'APP e, in generale, degli errori o dei problemi legati allo sviluppo o all'utilizzo degli elementi tecnici forniti all'utente dall'APP o da un programma.",

			"_SUBTITOL4": "FLUIDRA comunica che non garantisce:",

			"_AVIS6": "-Il funzionamento ininterrotto e privo di errori dell'accesso all'APP e/o ai siti di collegamento.",

			"_AVIS6.1": "-	L'assenza di errori di qualsiasi tipo, virus informatici e altri elementi nei contenuti o nel software a cui gli utenti			accedono tramite l'APP o nei siti di collegamento che possano provocare alterazioni nel sistema dell'utente, nei, documenti elettronici e negli archivi memorizzati nel rispettivo sistema informatico o che causino altri tipi di danni.",

			"_AVIS6.2": "Gli utenti considereranno le informazioni contenute nell'APP di natura divulgativa e orientativa sia in termini di			scopo che di effetto, motivo per cui FLUIDRA non garantisce l'esattezza di tali informazioni e, di conseguenza, non	assume alcuna responsabilità in merito ai possibili danni o disagi causati agli utenti per via di qualche inesattezza	ivi presente.",

			"_SUBTITOL5": "INFORMATIVA SULLA PRIVACY: PROTEZIONE DEI DATI PERSONALI",

			"_AVIS7": "Gli utenti si impegnano ad accedere all'APP e a utilizzarne il contenuto in buona fede.",

			"_AVIS7.1": "Ai sensi di quanto previsto dalla legge organica n. 15/1999 sulla protezione dei dati di carattere personale, informiamo che le informazioni di natura personale inserite nell'APP:",

			"_AVIS7.2": "1) Verranno trattate con la massima riservatezza e formeranno parte dei file di proprietà di FLUIDA S.A. a cui la società attinge per la gestione sia dell'APP che del rapporto di lavoro che la unisce all'utente.",

			"_AVIS7.3": "2) L'inserimento di tali dati implica l'accettazione della presente informativa sulla privacy, nonché l'autorizzazione in virtù della quale FLUIDRA potrà trattare i dati personali forniti.",

			"_AVIS7.4": "Conformemente alla legislazione vigente in materia di protezione dei dati, FLUIDRA ha adottato dei livelli di sicurezza adeguati ai dati forniti dagli utenti.Inoltre, ha impiegato tutti i mezzi e adottato tutte le misure a sua disposizione per evitare la perdita, il cattivo uso, l'alterazione, l'accesso non autorizzato e l'estrazione dei medesimi.",

			"_SUBTITOL6": "Diritti di accesso, rettifica, cancellazione e opposizione",

			"_AVIS8": "Gli utenti potranno esercitare in qualsiasi momento e conformemente a quanto stabilito dalla citata legge organica i	propri diritti di accesso, rettifica, cancellazione e opposizione relativamente ai rispettivi dati personali, rivolgendo un'istanza scritta in tal senso all'Ufficio Risorse Umane di Fluidra, all’indirizzo e- mail lopd@fluidra.com o al recapito postale: Avda. Francesc Macià 60, pl. 20, Sabadell(Barcelona), indicando, ove pertinente, in maniera visibile il diritto esercitato nel caso in questione e allegando una fotocopia del proprio documento d'identità.",

			"_SUBTITOL7": "GUIDA SULLA SICUREZZA D'USO:",

			"_SUBTITOL8": "Phishing",

			"_AVIS9": "Non utilizzare link riportati in e-mail o siti di terzi per accedere a questa APP. Periodicamente si rilevano casi di invio massivo di e- mail indiscriminate da indirizzi di posta elettronica falsi, con l'unico scopo di ottenere informazioni	riservate sugli utenti.Tale tecnica è nota come \"PHISHING\".FLUIDRA declina ogni responsabilità di terzi in tal senso",

			"_SUBTITOL9": "LEGGE APPLICABILE E FORO COMPETENTE",

			"_AVIS10": "L'accesso e l'utilizzo dell'APP vengono regolati e interpretati, conformemente alla legislazione spagnola. Qualsiasi controversia eventualmente sorta tra FLUIDRA e gli utenti dell'APP verrà risolta dai tribunali della città di Barcellona e le parti rinunciano così esplicitamente ad adire un proprio tribunale.",
			//End avis legal

			//Bases
			"_TITLE_BASES": "BASI DEL CONCORSO \"Our New Slogan\"",

			"_BASES1": "Obiettivo: in linea con la nuova mission di Fluidra (\"Prendersi cura del benessere e della salute delle persone attraverso un uso sostenibile dell'acqua nelle sue applicazioni ludiche, sportive e terapeutiche\"), l'obiettivo di questo concorso è quello di creare tutti insieme, ovvero con coloro che fanno parte di Fluidra, un nuovo slogan che meglio rifletta il senso più prossimo e umano della mission per sostituire così quello fino ad ora utilizzato (\"Making Water Perform\").",

			"_BASES2": "Tematica: il concorso consiste nella presentazione di proposte destinate a diventare il nuovo slogan del GRUPPO FLUIDRA mediante un'APP disponibile nelle seguenti lingue: spagnolo, inglese, francese, tedesco e italiano.",

			"_BASES3": "Per slogan (o claim) si intende una frase chiave o un'espressione in grado di identificare il valore distintivo della società. Si tratta di un motto capace di trasmettere un messaggio potente che rifletta l'essenza dell'azienda.",

			"_BASES4": "Partecipanti: potranno partecipare a questo concorso esclusivamente i dipendenti di tutte le aziende del GRUPPO FLUIDRA, fatte salvo due eccezioni: il personale dell'Ufficio Comunicazione aziendale e i membri della giuria.",

			"_BASES5": "Iscrizione: l'iscrizione è gratuita e si effettua tramite la creazione di un profilo utente nell'APP del concorso.",

			"_BASES6": "Accettazione di proposte, termini e requisiti: non esiste limite in quanto al numero di proposte per partecipante. Le lingue ammesse sono: spagnolo, inglese, francese, tedesco e italiano.",

			"_BASES7": "Si potranno pubblicare proposte fino al 30 settembre 2016. Parallelamente, i partecipanti potranno valutare positivamente le proposte degli altri esprimendo il proprio \"Mi piace\",enza restrizioni di numero.",

			"_BASES8": "La giuria valuterà le proposte pervenute a partire dall'1 ottobre 2016 e nel corso del mese di ottobre pubblicherà lo slogan vincitore.",

			"_BASES9": "Giuria: la giuria è costituita dal comitato aziendale.",

			"_BASES10": "Criteri applicati per la scelta dei progetti vincitori: pur tenendo in considerazione i \"Mi piace\" espressi dai partecipanti, tale fattore non sarà vincolante, per cui nella scelta dello slogan vincitore la giuria terrà conto di quanto segue:",

			"_BASES10.1": "- Le proposte più originali e creative che esprimono al meglio i valori aziendali e la nuova mission di Fluidra: prendersi cura del benessere e della salute delle persone attraverso un uso sostenibile dell'acqua nelle sue applicazioni ludiche, sportive e terapeutiche.",

			"_BASES10.2": "- La proposta vincitrice dovrà avere senso compiuto e suonare bene in inglese.",

			"_BASES11": "Qualora la giuria non dovesse reputare adeguata nessuna proposta per rappresentare lo slogan del GRUPPO FLUIDRA il concorso potrebbe dichiararsi senza esito. Il GRUPPO FLUIDRA si riserva il diritto di apportare qualsiasi modifica, cambiamento e/o adattamento alla proposta vincitrice.",

			"_BASES12": "Premio: la pubblicazione della proposta vincitrice avverrà tramite l'APP, il sito livingfluidra.fluidra.com e l'invio di un'e-mail al vincitore.",

			"_BASES13": "Il premio è costituito da un buono regalo per due persone che dà diritto a trascorrere un week-end in un hotel wellness dal valore stimato di euro 1.000.",

			"_BASES14": "Il premio sarà consegnato da un dirigente di Fluidra. Inoltre, il vincitore sarà intervistato dalla rivista on-line di Fluidra \"LivingFluidra\".",

			"_BASES15": "Il bonus non potrà essere cambiato per il corrispettivo in contanti.",

			"_BASES16": "Accettazione e squalifica: nel momento in cui si scarica l'APP del concorso e si crea di conseguenza un profilo per il medesimo si accettano espressamente tutti i punti indicati nelle presenti basi del concorso, nonché la decisione inappellabile della giuria. L'inadempimento di tali basi provocherà la squalifica del partecipante.",

			"_BASES17": "Protezione dei dati personali: conformemente a quanto disposto dalla normativa vigente in materia di protezione dei dati di carattere personale, la società FLUIDRA S.A., con sede in Av. Francesc Macià n. 60, planta 20, 08208 Sabadell, Barcelona, appartenente al GRUPO FLUIDRA, ricorda ai partecipanti che i rispettivi dati personali vengono inseriti in un archivio creato e gestito sotto l'esclusiva responsabilità di FLUIDRA S.A. allo scopo di sviluppare l'attività dell'azienda.",

			"_BASES18": "Inoltre, ribadisce che i dati personali dei partecipanti potranno essere ceduti alle diverse aziende che costituiscono il GRUPPO FLUIDRA, che questi potranno consultare in qualsiasi momento attraverso l'Intranet dell'azienda, allo stesso scopo e che tale cessione è necessaria per lo sviluppo, l'adempimento e il controllo del rispettivo rapporto lavorativo con la società. Qualora un partecipante non fosse d'accordo con quanto sopra potrà esercitare in qualsiasi momento i propri diritti di accesso, rettifica, cancellazione e/o opposizione via e-mail, scrivendo a lopd@fluidra.com.",

			"_BASES19": "Materiale presentato: il partecipante dichiara l'originalità dello slogan presentato durante il concorso, poiché lo ha creato egli stesso. Lo slogan non è pertanto una copia né un plagio.",

			"_BASES20": "Il partecipante cede integralmente a FLUIDRA S.A. qualsiasi diritto di proprietà intellettuale e industriale derivata dagli slogan proposti, che vincano o meno.",

			"_BASES21": "La cessione a cui fa riferimento il presente documento è esclusiva e il suo ambito territoriale è mondiale. La durata della citata cessione corrisponde al tempo massimo disposto dalla legislazione internazionale applicabile.",
			//End Bases


			//claims
			"Cargando más claims...": "Caricamento altri claim in corso…",
			//End claims	

			//dash
			"Participar": "Partecipa",

			"Mi perfil": "Il mio profilo",

			"Eslóganes": "Slogan",

			"Top 5": "Top 5",

			"Bases del concurso": "Basi del concorso",

			"Aviso legal": "Avvertenza legale",
			//End dash

			//firsttime
			"¡Bienvenido a Fluidra Slogans!": "Benvenuti a Fluidra Slogans!",

			"Queremos explicarte un poco de que va esta app.": "Vogliamo spiegarti brevemente in cosa consiste questa app",

			"Participa": "Partecipa",

			"Desde esta app podrás participar aportando tus ideas para nuestro nuevo slogan. Los mejores se llevaran un buen premio.": "Tramite questa app potrai contribuire al nostro nuovo slogan, apportando le tue idee. I migliori vinceranno un fantastico premio.",

			"Valora": "Valuta",

			"Valora las publicaciones de otros usuarios dandole al like.": "Valuta le pubblicazioni di altri utenti, facendo clic su like",

			"Disfruta": "Divertiti",

			"Sobretodo, disfruta de esta experiencia.": "Soprattutto, goditi questa esperienza.",

			"Continuar": "Continua",
			//End firsttime

			//login
			"¡Buscamos un nuevo eslogan para Fluidra!": "Cerchiamo un nuovo slogan per Fluidra!",

			"Participa en este emocionante concurso proponiendo eslóganes y valorando los de tus compañeros.": "Partecipa a questo emozionante concorso, proponendo slogan e valutando quelli degli altri.",

			"Entre todos podemos encontrar el mejor eslogan ¿Será el tuyo? Participa.": "Tutti insieme possiamo trovare lo slogan migliore. Sarà il tuo a vincere? Partecipa.",

			"Registrarse": "Registrarsi",

			"Ya estoy registrado": "Sono già registrato",

			"¿Olvidaste la contraseña?": "Hai dimenticato la password?",

			"Recuperación de contraseña": "Recupero della password",

			"Escriba el correo electrónico con el que se dió de alta y le enviaremos un enlace para resetear su contraseña.": "Scrivere l’indirizzo e-mail con cui ci si è registrati per ricevere il link che consente di ripristinare la password.",

			"Correo electrónico": "E-mail",

			"Cancelar": "Cancella",

			"Aceptar": "Accetta",

			"Atención": "Attenzione",

			"Email o contraseña incorrectas.": "E-mail o password errate",

			"Acceder": "Entra",

			"Escriba su correo electrónico y contraseña.": "Scrivere e-mail e password",

			"Contraseña": "Password",
			//End login

			//participa
			"OK": "OK",

			"Escribe un nuevo eslogan...": "Scrivere un nuovo slogan…",

			"Publicando...": "Editoria ...",

			"No se ha podido publicar. Intentelo más tarde.": "Non poteva pubblicare. Si prega di riprovare più tardi.",

			"¡Publicado!": "Pubblicato!",

			"Se ha publicado tu propuesta, muchas gracias por participar.": "La tua proposta è stato pubblicato, vi ringrazio molto per la partecipazione.",
			//End participa

			//preauth
			"Para poder usar esta app debes introducir el código de autorización.": "Per poter usare questa app è necessario inserire il codice di autorizzazione.",

			"Escanear QR": "",

			"No se ha podido escanear correctamente el código.": "Impossibile scannerizzare correttamente il codice.",

			"Este código no es correcto": "Questo codice non è corretto.",
			//End preauth

			//preregistre
			"Acepto las condiciones": "Accetto le condizioni",

			"Condiciones y bases del concurso": "Condizioni e basi del concorso",
			//End preregistre

			//profile
			"Nombre": "Nome",

			"Apellidos": "Cognome",

			"Email": "E-mail",

			"Empresa": "Ragione sociale",

			"Idioma": "Lingua",

			"Español": "Español",

			"Italiano": "Italiano",

			"Deutsch": "Deutsch",

			"English": "English",

			"Français": "Français",

			"País": "Paese",
			
			"Cerrar sesión" : "logoff",

			"No hemos podido guardado las modificaciones.": "Impossibile salvare le modifiche.",

			"No hemos podido usar la imagen.": "Impossibile usare l’immagine.",

			"¡Guardado!": "Salvato!",

			"Se han guardado las modificaciones.": "Modifiche salvate.",

			"Escoger de la galería": "Scegli un’immagine dalla gallery",

			"Camara de fotos": "Fotocamera",

			"Añadir foto desde...": "Aggiungi foto da…",
			//End profile

			//ranking
			// NO DATA
			//End ranking

			//tabs
			"Añadir": "Aggiungi",
			//End tabs
		},
		'fr': {

			'Usuarios': 'Utilisateurs',

			'usuarios': 'utilisateurs',

			'encontrados': 'trouvés',

			'Actividad': 'Activité',

			'Buscar': 'Chercher',

			'CÓDIGO': 'CODE',
			
			'Míos': 'Mes slogans',
			
			//Avis legal
			"_TITLE1": "MENTION LÉGALE ET CONDITIONS D'UTILISATION DE L'APPLICATION",

			"_AVIS1": "FLUIDRA S.A, (ci-après désignée « FLUIDRA »), sise à Avda. Francesc Macià 60, pl. 20 de Sabadell (Barcelone), inscrite au Registre du Commerce de Barcelone, au tome 36883, feuillet 132, page n° B 290316, pourvue du CIF A-17728593, titulaire de l'application « Our New Slogan » (ci-après désigné « l'Application »).",

			"_TITLE2": "ACCÈS ET CONDITIONS D'UTILISATION DE L'APPLICATION",

			"_AVIS2": "Les présentes conditions d'utilisation (ci-après désignées « les Conditions ») ont pour but d'expliquer à l'utilisateur de l'application (ci-après « l'Utilisateur ») son fonctionnement.",

			"_AVIS2.1": "L'accès et l'utilisation de l'Application impliquent l'acceptation par l'Utilisateur, sans aucune réserve, de toutes et chacune des présentes Conditions. Si vous n'acceptez pas les présentes conditions, nous vous prions de vous	abstenir d'utiliser cette Application.",

			"_AVIS2.2": "FLUIDRA pourra à tout moment, et sans préavis, modifier les présentes Conditions, et la totalité du contenu de	l'Application, en y publiant ces modifications, afin que les Utilisateurs puissent en avoir connaissance avant d'utiliser l'Application.",

			"_AVIS2.3": "FLUIDRA pourra refuser l'accès à l'Application aux Utilisateurs qui feront un mauvais usage des contenus et/ou qui	manqueraient à l'une des conditions qui apparaissent dans le présent document.",

			"_SUBTITOL1": "Accès et Utilisation",

			"_AVIS3": "L'accès à l'Application est gratuit, hormis le coût de la connexion à travers le réseau des télécommunications du fournisseur d'accès choisi par l'Utilisateur.",

			"_AVIS3.1": "L'utilisateur s'engage à faire une utilisation diligente de l'Application, ainsi que des informations qui y apparaissent,		en se soumettant entièrement à la réglementation applicable, ainsi qu'aux présentes Conditions.",

			"_SUBTITOL2": "Obligation d'en faire un usage correct",

			"_AVIS4": "Les Utilisateurs sont entièrement responsables de leur conduite lorsqu'ils accèdent aux informations de l'Application, pendant qu'ils l'utilisent, et après l'avoir quittée.",

			"_AVIS4.1": "L'Utilisateur s'engage à réaliser une correcte utilisation de l'Application et des fonctionnalités qui lui sont fournies,		conformément à la Loi, aux présentes Conditions, aux instructions et avertissements qui lui sont communiqués,ainsi qu'à la morale et les bonnes mœurs généralement acceptés et à l'ordre public.",

			"_AVIS4.2": "L'utilisateur s'engage à utiliser exclusivement l'Application ainsi que tous ses contenus, à des fins licites et non prohibées, n'enfreignant pas la loi en vigueur et/ou pouvant violer les droits légitimes du GROUPE FLUIDRA ou de tout tiers et/ou pouvant causer des dommages et des préjudices de manière directe ou indirecte.",

			"_AVIS4.3": "Conformément aux dispositions précédentes, on entendra par contenu, de manière non exhaustive : les textes,			photographies, graphiques, images, icônes, technologies logiciels, liens et autres contenus audiovisuels ou	sonores, ainsi que leur conception graphique et codes sources (ci-après l'ensemble désigné « le/les contenu/s »), conformément à la loi, aux présentes Conditions, et autres avertissements, règlements d'usage et instructions portées à votre connaissance, ainsi que la morale et les bonnes mœurs généralement acceptées, et l'ordre public, et en particulier, vous vous engagez à vous abstenir de : reproduire, copier, distribuer, mettre à disposition ou de toute autre forme communiquer publiquement, transformer ou modifier les Contenus, à moins de disposer de l'autorisation du titulaire des droits correspondants, ou que cela soit légalement autorisé.",

			"_AVIS4.5": "L'utilisateur s'engage, à titre purement énonciatif et non exhaustif, à ne pas transmettre, diffuser ou mettre à la			disposition de tiers des informations, des données, contenus, messages, graphiques, dessins, fichiers de son et/ou d'image, photographies, enregistrements, logiciels et en général, toute sorte de matériel appartenant à cette Application, ainsi qu'à s'abstenir de réaliser des actes, qui :",

			"_AVIS4.6": "- Induisent, incitent ou encouragent à des actions délictuelles, diffamatoires, violentes, ou en général, contraires à la loi, à la morale ou aux bonnes mœurs, généralement acceptées ou à l'ordre public.",

			"_AVIS4.7": "- Soient faux, ambigus, inexacts, exagérés ou extemporanés, de manière à ce qu'ils induisent ou puissent induire à l'erreur sur leur objet ou sur les intentions ou la volonté du communicant :",

			"_AVIS4.8": "-	Sont protégés par un droit de propriété intellectuelle ou industrielle appartenant à des tiers, sans que l'utilisateur ait obtenu préalablement l'autorisation nécessaire pour procéder à l'utilisation qu'il en fait ou prétend en faire ;",

			"_AVIS4.9": "- Constituent, selon le cas, de la publicité illégale, mensongère ou déloyale et, en général, constituant de la concurrence déloyale ou enfreignant la réglementation relative à la protection des données à caractère personnel.",

			"_AVIS4.10": "-	Intègrent un virus ou tout autre élément physique ou électronique pouvant endommager ou empêcher le	fonctionnement normal du réseau, du système ou des équipements informatiques (matériel et logiciel) de FLUIDRA.",

			"_AVIS4.11": "-	Provoquent de par leurs caractéristiques (telles que le format ou l'extension, entre autres), des difficultés dans	le normal fonctionnement des services proposés par cette Application.",

			"_AVIS4.12": "L'utilisateur répondra des dommages et des préjudices de toute nature que FLUIDRA pourra subir en conséquence	du manquement à toute obligation auxquelles il est soumis en vertu des présentes Conditions ou de la législation applicable, en relation avec l'utilisation de l'Application.",

			"_SUBTITOL3": "EXONÉRATION DE LA RESPONSABILITÉ DE FLUIDRA",

			"_AVIS5": "À titre énonciatif, mais non limitatif, FLUIDRA ne sera pas tenue responsable de :",

			"_AVIS5.1": "- L'utilisation que les utilisateurs puissent faire des matériels contenus dans cette Application, qu'ils soient interdits ou autorisés, en infraction des droits de propriété intellectuelle et/ou industrielles des contenus du site ou	de tiers.",

			"_AVIS5.2": "- Des éventuels dommages et préjudices aux utilisateurs causés par un fonctionnement normal ou anormal des outils de recherche, de l'organisation ou de la localisation des contenus et/ou accès à l'Application et, en général, des erreurs ou des problèmes qui sont créés dans le développement ou l'instrumentation des éléments techniques que l'Application ou un programme fournit à l'utilisateur.",

			"_SUBTITOL4": "FLUIDRA informe qu'elle ne garantit pas :",

			"_AVIS6": "- Que l'accès à l'Application, et/ou aux sites Web associés soient ininterrompus ou sans erreurs.",

			"_AVIS6.1": "-	Que le contenu ou le logiciel auquel les utilisateurs accèdent à travers l'Application, ou les sites Web associés, ne contiennent pas d'erreur, de virus informatique ou tout autre élément dans les contenus pouvant	produire des altérations dans leur système ou dans les documents électroniques et les fichiers stockés dans leur système informatique ou causer tout autre type de dommage;",

			"_AVIS6.2": "Les informations apparaissant dans l'Application doivent être considérées par les utilisateurs comme des informations fournies à titre indicatif, tant pour ce qui est de la finalité que de leurs effets, raison pour laquelle FLUIDRA ne garantit pas l'exactitude de ces informations, et donc dégage toute responsabilité pour les éventuels préjudices et problèmes pouvant découler pour l'utilisateur d’une quelconque inexactitude.",

			"_SUBTITOL5": "POLITIQUE DE CONFIDENTIALITÉ : PROTECTION DES DONNÉES PERSONNELLES",

			"_AVIS7": "Les utilisateurs s'engagent à accéder à l'Application ou à utiliser le contenu en toute bonne foi.",

			"_AVIS7.1": "En application des dispositions de la Loi organique 15/1999, sur la Protection des données à caractère	personnel, nous vous informons que les données personnelles intégrées à l'Application :",

			"_AVIS7.2": "1 .- Seront traitées en toute confidentialité, et feront partie des fichiers appartenant à FLUIDRA S.A., aux fins de la			gestion de l'Application, ainsi que pour la gestion de la relation professionnelle qui les lient.",

			"_AVIS7.3": "2 .- Implique l'acceptation de cette politique de confidentialité, ainsi que l'autorisation pour FLUIDRA de traiter les			données personnelles qui lui sont fournies.",

			"_AVIS7.4": "Conformément à la législation en vigueur en matière de protection des données, FLUIDRA a adopté les niveaux de	sécurité adaptés aux données fournies par les utilisateurs, et a installé, en outre, tous les moyens et toutes les mesures à sa portée afin d'éviter la perte, le mauvais usage, l'altération, l'accès non autorisé et l'extraction de ces dernières.",

			"_SUBTITOL6": "Droits d'accès, de rectification, d'annulation et d'opposition",

			"_AVIS8": "À tout moment, et conformément aux dispositions de la loi organique, les utilisateurs pourront exercer les droits			d'accès, de rectification, d'annulation et d'opposition de leurs données personnelles, sur demande écrite dirigée à l'adresse électronique lopd@fluidra.com ou à l'adresse postale : Avda. Francesc Macià 60, pl. 20 de Sabadell, (Barcelone), au Département des Ressources humaines, en indiquant selon le cas, de manière visible le droit concret exercé, et en joignant une copie de leur Carte Nationale d’Identité.",

			"_SUBTITOL7": "GUIDE D'UTILISATION SÉCURISÉE :",

			"_SUBTITOL8": "Hameçonnage",

			"_AVIS9": "N'utilisez pas les liens intégrés dans les courriels ou les sites Web de tiers pour accéder à cette Application. Des			envois de courriers massifs indistincts sont régulièrement détectés, ils sont envoyés à partir de fausses adresses électroniques, dans le seul but d'obtenir des informations confidentielles des utilisateurs. Cette technique est connue sous le nom de hameçonnage. FLUIDRA décline toute responsabilité à cet égard.",

			"_SUBTITOL9": "LÉGISLATION APPLICABLE ET JURIDICTION",

			"_AVIS10": "L'accès et l'utilisation de l’Application seront régis et interprétés conformément à la législation espagnole. Tout désaccord pouvant apparaître entre FLUIDRA et les utilisateurs de l'Application sera résolu, renonçant expressément à tout autre for, par les Tribunaux de la ville de Barcelone.",
			//End avis legal

			//Bases
			"_TITLE_BASES": "BASE DU CONCOURS \"Our New Slogan\"",

			"_BASES1": "Objectif : Dans la ligne de la nouvelle Mission de Fluidra « S'occuper du bien-être et de la santé des personnes à travers un usage durable de l'eau dans ses applications ludiques et thérapeutiques », l'objectif de ce concours est de trouver avec l'ensemble des intégrants de Fluidra, un nouveau slogan reflétant au mieux le sens le plus proche et le plus humain de la Mission, et pour remplacer l'actuel « Making Water Perform ».",

			"_BASES2": "Sujet : Le concours consistera en la présentation de propositions d'un nouveau slogan pour le GROUPE FLUIDRA au moyen d'une Application disponible dans les langues suivantes : espagnol, anglais, français, allemand et italien.",

			"_BASES3": "On entend par slogan une phrase clé, ou une expression identifiant la valeur distinctive de l'entreprise. Vous avez la possibilité de transmettre un message puissant exprimant l'essence de la société.",

			"_BASES4": "Participants : Pourront participer à ce concours uniquement les employés de toutes les entreprises du GROUPE FLUIDRA, à deux exceptions près : les membres du Département de la Communication corporative, ainsi que les membres du jury.",

			"_BASES5": "Inscription : L'inscription est gratuite, et est réalisée moyennant la création d'un profil d'utilisateur sur l'Application du concours.",

			"_BASES6": "Admission des propositions, délais et conditions requises : Il n'y aura pas de limite au nombre de propositions pour chaque participant. Les langues autorisées sont l'espagnol, l'anglais, le français, l'allemand et l'italien.",

			"_BASES7": "La publication de propositions sera admise jusqu'au 30 septembre 2016. Parallèlement, les participants pourront exprimer un « J'aime » pour les propositions des autres participants qu'ils apprécient.",

			"_BASES8": "Le jury évaluera les propositions à partir du 1 er octobre 2016 et pendant le mois d'octobre le slogan gagnant sera publié.",

			"_BASES9": "Jury : Le jury sera composé du Comité corporatif.",

			"_BASES10": "Criteri applicati per la scelta dei progetti vincitori: pur tenendo in considerazione i \"Mi piace\" espressi dai partecipanti, tale fattore non sarà vincolante, per cui nella scelta dello slogan vincitore la giuria terrà conto di quanto segue:",

			"_BASES10.1": "- Les propositions les plus originales et créatives exprimant les valeurs corporatives ainsi que la nouvelle mission de Fluidra : S'occuper du bien-être et de la santé des personnes à travers un usage durable de l'eau dans ses applications ludiques, sportives et thérapeutiques.",

			"_BASES10.2": "- La proposition gagnante devra avoir un sens et bien sonner en anglais.",

			"_BASES11": "Le concours pourra être annulé, si le jury ne trouve aucune proposition adaptée pour représenter le slogan du GROUPE FLUIDRA. Le GROUPE FLUIDRA se réserve le droit de réaliser tout changement et/ou modification ou adaptation de la proposition gagnante.",

			"_BASES12": "Prix : La publication de la proposition gagnante sera réalisée dans l'Application, sur le site livingfluidra.fluidra.com et communiquée par courrier électronique au gagnant.",

			"_BASES13": "Le prix est un bon pour un week-end dans un hôtel bien-être pour 2 personnes, évalué à 1000 euros.",

			"_BASES14": "Remise du prix par un haut responsable de Fluidra, ainsi qu'une interview dans le magazine en ligne de Fluidra « LivingFluidra ».",

			"_BASES15": "Le prix ne pourra être échangé contre des espèces.",

			"_BASES16": "Acceptation et disqualification : Le téléchargement de l'Application du concours et la création subséquente d'un profil dans celui-ci, impliquent l'acceptation formelle de tous et chacun des points indiqués dans ces bases, et l'acceptation de la décision sans appel du jury. Le manquement à ces bases donnera lieu à la disqualification du participant.",

			"_BASES17": "Protection des données personnelles : Aux effets des dispositions de la réglementation en vigueur relative à la protection des données à caractère personnel, la société FLUIDRA S.A. sise à Av. Francesc Macià, 60, planta 20, 08208 Sabadell, Barcelone, appartenant au GROUPE FLUIDRA, vous rappelle que vos données personnelles ont été intégrées à un fichier créé par et sous la responsabilité de FLUIDRA S.A., à des fins de développement de l'activité de la Société.",

			"_BASES18": "Nous vous rappelons également que vos données personnelles pourront faire l'objet d'une cession aux différentes sociétés du GROUPE FLUIDRA, que vous pourrez consulter à tout moment à travers l'intranet de l'entreprise, aux mêmes fins et que cette cession est nécessaire au développement, accomplissement et contrôle de votre relation contractuelle avec la société. Si vous n'êtes pas d'accord avec ce qui précède, vous pouvez exercer à tout moment vos droits d'accès, de rectification, d'annulation et/ou d'opposition moyennant un courrier électronique adressé à lopd@fluidra.com.",

			"_BASES19": "Matériel présenté : Le participant reconnaît que le slogan présenté au concours est original, de sa propre création et qu'il n'implique ni copie ni plagiat.",

			"_BASES20": "Le Participant cède intégralement en faveur de FLUIDRA S.A. tout droit de propriété intellectuelle et/ou industrielle pouvant découler de l'adoption des slogans proposés, qu'ils soient gagnants ou pas.",

			"_BASES21": " La cession à laquelle ce document fait référence est exclusive, et sa portée territoriale est mondiale. La durée de cette cession comprend le maximum indiqué par la législation internationale applicable.",
			//End Bases

			//claims
			"Cargando más claims...": "Téléchargement d'autres slogans...",
			//End claims	

			//dash
			"Participar": "Participer",

			"Mi perfil": "Mon profil",

			"Eslóganes": "Slogans",

			"Top 5": "Top 5",

			"Bases del concurso": "Bases du concours",

			"Aviso legal": "Mention légale",
			//End dash

			//firsttime
			"¡Bienvenido a Fluidra Slogans!": "Bienvenue chez Fluidra Slogans !",

			"Queremos explicarte un poco de que va esta app.": "Nous allons vous expliquer un peu ce qu'est cette application",

			"Participa": "Participez",

			"Desde esta app podrás participar aportando tus ideas para nuestro nuevo slogan. Los mejores se llevaran un buen premio.": "À travers cette application, vous pourrez participer en apportant vos idées pour notre nouveau slogan.  Les meilleurs remporteront une belle récompense",

			"Valora": "Évaluez",

			"Valora las publicaciones de otros usuarios dandole al like.": "Évaluez les publications des autres utilisateurs en appuyant sur Like",

			"Disfruta": "Profitez",

			"Sobretodo, disfruta de esta experiencia.": "Surtout, profitez de cette expérience",

			"Continuar": "Continuer",
			//End firsttime

			//login
			"¡Buscamos un nuevo eslogan para Fluidra!": "Nous recherchons un nouveau slogan pour Fluidra !",

			"Participa en este emocionante concurso proponiendo eslóganes y valorando los de tus compañeros.": "Participez à cet excitant concours en proposant des slogans et en évaluant ceux de vos collègues",

			"Entre todos podemos encontrar el mejor eslogan ¿Será el tuyo? Participa.": "Tous ensemble, nous pouvons trouver le meilleur slogan, s'agira-t-il du vôtre ? Participez.",

			"Registrarse": "Enregistrez-vous",

			"Ya estoy registrado": "Je suis déjà enregistré",

			"¿Olvidaste la contraseña?": "Vous avez oublié votre mot de passe ?",

			"Recuperación de contraseña": "Récupération du mot de passe",

			"Escriba el correo electrónico con el que se dió de alta y le enviaremos un enlace para resetear su contraseña.": "Saisissez l'adresse électronique avec laquelle vous vous êtes inscrit/e, et nous vous enverrons un lien pour réinitialiser votre mot de passe",

			"Correo electrónico": "Courrier électronique",

			"Cancelar": "Annuler",

			"Aceptar": "Accepter",

			"Atención": "Attention",

			"Email o contraseña incorrectas.": "Adresse électronique ou mot de passe incorrect.",

			"Acceder": "Accéder",

			"Escriba su correo electrónico y contraseña.": "Saisissez votre adresse électronique et mot de passe",

			"Contraseña": "Mot de passe",
			//End login

			//participa
			"OK": "OK",

			"Escribe un nuevo eslogan...": "Écrivez un nouveau slogan...",

			"Publicando...": "Édition...",

			"No se ha podido publicar. Intentelo más tarde.": "Il ne pouvait pas publier. S'il vous plaît réessayer plus tard.",

			"¡Publicado!": "Posté !",

			"Se ha publicado tu propuesta, muchas gracias por participar.": "Votre proposition a été publiée, je vous remercie beaucoup d'avoir participé.",
			//End participa

			//preauth
			"Para poder usar esta app debes introducir el código de autorización.": "Pour pouvoir utiliser cette application, vous devez saisir le code d'autorisation.",

			"Escanear QR": "",

			"No se ha podido escanear correctamente el código.": "Le code n'a pas pu être scanné correctement",

			"Este código no es correcto": "Ce code n'est pas correct",

			"Acepto las condiciones": "J'accepte les conditions",

			"Condiciones y bases del concurso": "Conditions et bases du concours",
			//End preregistre

			//profile
			"Nombre": "Prénom",

			"Apellidos": "Nom",

			"Email": "Adresse électronique",

			"Empresa": "Entreprise",

			"Idioma": "Langue",

			"Español": "Español",

			"Italiano": "Italiano",

			"Deutsch": "Deutsch",

			"English": "English",

			"Français": "Français",

			"País": "Pays",
			
			"Cerrar sesión" : "logoff",

			"No hemos podido guardado las modificaciones.": "Nous n'avons pas pu sauvegarder les modifications",

			"No hemos podido usar la imagen.": "Nous n'avons pas pu utiliser l'image",

			"¡Guardado!": "Sauvegardé !",

			"Se han guardado las modificaciones.": "Les modifications ont été sauvegardées.",

			"Escoger de la galería": "Choisir dans la galerie",

			"Camara de fotos": "Appareil photo",

			"Añadir foto desde...": "Ajouter photo à partir de...",
			//End profile

			//ranking
			// NO DATA
			//End ranking

			//tabs
			"Añadir": "Ajouter"
			//End tabs
		},
		'de': {

			'Usuarios': 'Nutzer',

			'usuarios': 'nutzer',

			'encontrados': 'gefunden',

			'Actividad': 'Aktivität',

			'Buscar': 'Suchen',

			'CÓDIGO': 'CODE',
			
			'Míos': 'Meine Slogans',
			
			//Avis legal
			"_TITLE1": "RECHTLICHER HINWEIS UND NUTZUNGSBEDINGUNGEN FÜR DIE APP",

			"_AVIS1": "FLUIDRA S.A. (im Folgenden „FLUIDRA“ ) mit Gesellschaftssitz in Avda. Francesc Macià 60, pl. 20 in Sabadell (Barcelona), eingetragen ins Handelsregister von Barcelona in Band 36883, Foliant 132, Registerseite B 290316, mit USt.-ID A-17728593, ist Eigentümerin der App „ Our New Slogan “ (im Folgenden „ die App“ ).",

			"_TITLE2": "ZUGRIFF AUF UND NUTZUNGSBEDINGUNGEN FÜR DIE APP",

			"_AVIS2": "Diese Nutzungsbedingungen (im Folgenden die „ Bedingungen “ ) dienen dem Zweck, den Benutzern der App (im Folgenden der „ Benutzer “ ) deren Funktionsweise zu erläutern",

			"_AVIS2.1": "Der Zugriff auf und die Nutzung der App bedeuten die vorbehaltlose Annahme jeder der hierin enthaltenen Bedingungen durch den Benutzer. Wenn Sie diese Bedingungen nicht akzeptieren, bitten wir Sie, von der Nutzung der App abzusehen",

			"_AVIS2.2": " FLUIDRA kann diese in der App aufgeführten Bedingungen jederzeit und ohne Vorankündigung ändern, indem die Änderungen in der App veröffentlicht werden, damit die Benutzer sie einsehen können, bevor sie die App nutzen",

			"_AVIS2.3": " FLUIDRA kann denjenigen Benutzern, die die Inhalte der App nicht sachgerecht verwenden und/oder gegen die in diesem Dokument aufgeführten Bedingungen verstoßen, den Zugriff auf die App verweigern.",

			"_SUBTITOL1": "Zugriff und Nutzung",

			"_AVIS3": "Der Zugriff auf die App ist kostenlos, mit Ausnahme der Kosten der Verbindung über das Telekommunikationsnetz, die vom Internetdienstanbieter des Benutzers bereitgestellt wird",

			"_AVIS3.1": " Der Benutzer verpflichtet sich, die App und die darin enthaltenen Informationen sachgerecht zu verwenden und sich jederzeit sowohl an die geltenden Vorschriften als auch an diese Bedingungen zu halten.",

			"_SUBTITOL2": "Verpflichtung zur sachgerechten Verwendung",

			"_AVIS4": "Die Benutzer sind voll für ihr Verhalten beim Zugriff auf die Informationen dieser App während der Nutzung sowie im Anschluss an den Zugriff verantwortlich",

			"_AVIS4.1": " Der Benutzer verpflichtet sich, die ihm bereitgestellte App und deren Tools unter Einhaltung der Gesetze, dieser Bedingungen, der ihm mitgeteilten Anweisungen und Hinweise sowie der allgemein akzeptierten Moral und guten Sitten sowie der öffentlichen Ordnung zu verwenden",

			"_AVIS4.2": "Der Benutzer verpflichtet sich, die App und alle ihre Inhalte ausschließlich zu rechtmäßigen und nicht verbotenen Zwecken zu  erwenden, die nicht gegen geltende Gesetze verstoßen oder die berechtigten Rechte der FLUIDRA- GRUPPE oder von Dritten verletzen und/oder direkte oder indirekte Schäden verursachen können",

			"_AVIS4.3": " In Übereinstimmung mit den obigen Ausführungen versteht sich unter Inhalten Folgendes (mit aufzählendem und nicht einschränkenden Charakter)",

			"_AVIS4.4": " Texte, Fotografien, Grafiken, Bilder, Symbole, Technologie, Software, Links und andere audiovisuelle oder Audioinhalte sowie deren Grafikdesign und Quellcode (im Folgenden der „ Inhalt “ ), in Übereinstimmung mit dem Gesetz, diesen Bedingungen, den sonstigen Hinweisen, Bedienungsanleitungen und Anweisungen, die mitgeteilt wurden, sowie mit der allgemein akzeptierten Moral und den guten Sitten und der öffentlichen Ordnung. Insbesondere verpflichtet sich der Benutzer, vom Wiedergeben, Kopieren, Verteilen, Verfügbarmachen oder jeder sonstigen öffentlichen Kommunikation, Umwandlung oder Änderung des Inhalts abzusehen, es sei denn, er verfügt über die Genehmigung des Inhabers der jeweiligen Rechte oder dies ist rechtlich zulässig",

			"_AVIS4.5": " Der Benutzer verpflichtet sich mit aufzählendem, aber nicht  inschränkendem Charakter, keine Daten, Inhalte, Nachrichten, Grafiken, Zeichnungen, Audio- oder Bilddateien, Fotografien, Aufzeichnungen, Software und generell alle Arten von Material, die Eigentum dieser App sind, an Dritte zu übertragen, sie zu verbreiten oder bereitzustellen, sowie von Handlungen abzusehen, die",

			"_AVIS4.6": " - zu deliktiven, verleumderischen oder gewalttätigen Handlungen oder generell solchen, die gegen das Gesetz, die Moral und die allgemein akzeptierten guten Sitten oder die öffentliche Ordnung verstoßen, verleiten, dazu anregen oder diese fördern.",

			"_AVIS4.7": "- falsch, zweideutig, ungenau, übertrieben oder unangemessen sind und dadurch zu Fehlern bezüglich des Gegenstands oder der Absicht oder Vorschläge des Mitteilenden verleiten oder verleiten können",

			"_AVIS4.8": " - von jeglichen Rechten am geistigen oder industriellen Eigentum von Dritten geschützt sind, ohne dass der Benutzer zuvor von den Inhabern die erforderliche Genehmigung zur geplanten oder getätigten Nutzung erwirkt hat",

			"_AVIS4.9": " - ggf. ungesetzliche, täuschende oder unlautere Werbung bilden und generell unlauteren Wettbewerb darstellen oder gegen die Bestimmungen zum Schutz von personenbezogenen Daten verstoßen",

			"_AVIS4.10": " - Viren oder andere physische oder elektronische Elemente einführen, die das Netz, das System oder die Computeranlagen (Hardware und Software) von FLUIDRA beschädigen oder dessen regulären Betrieb stören oder verhindern können",

			"_AVIS4.11": " - aufgrund ihrer Eigenschaften (u.a. Format oder Erweiterung) Probleme beim normalen Betrieb der von dieser App gebotenen Services verursachen",

			"_AVIS4.12": " Der Benutzer haftet für die Schäden und Nachteile jeglicher Art, die FLUIDRA infolge der Nichterfüllung jeglicher Verpflichtungen erleidet, denen der Benutzer in Anwendung dieser Bedingungen bzw. der anwendbaren Gesetze bei Nutzung der App unterliegt.",

			"_SUBTITOL3": "HAFTUNGSFREISTELLUNG VON FLUIDRA",

			"_AVIS5": "FLUIDRA übernimmt keinerlei Haftung für Folgendes (mit aufzählendem, nicht einschränkendem Charakter):",

			"_AVIS5.1": "- die Nutzung der in dieser App enthaltenen Materialien seitens der Benutzer, seien sie verboten oder zulässig, mit der gegen die Rechte am geistigen und/oder industriellen  igentum der Inhalte der App oder von Dritten verstoßen wird",

			"_AVIS5.2": " - etwaige Schäden, die den Benutzern durch den normalen oder fehlerhaften Betrieb der Suchinstrumente, der Organisation oder des Speicherorts der Inhalte und/oder den Zugriff auf die App entstehen, sowie allgemein aus allen Fehlern oder Problemen, die bei der Entwicklung oder Instrumentierung der technischen Elemente entstehen, welche die App oder ein Programm dem Benutzer bereitstellen.",

			"_SUBTITOL4": "FLUIDRA weist darauf hin, dass nicht garantiert wird:",

			"_AVIS6": "- dass der Zugriff auf die App oder die verlinkten Websites ununterbrochen oder fehlerfrei zur Verfügung steht",

			"_AVIS6.1": " - dass der Inhalt oder die Software, auf die der Benutzer über die App oder die verlinkten Websites zugreift, keine Fehler, Computerviren oder andere Elemente in ihren Inhalten enthält, die zu Störungen auf seinem System oder den auf seinem Computersystem gespeicherten elektronischen Dokumenten und Dateien führen oder Schäden anderer Art anrichten können",

			"_AVIS6.2": " Die in dieser App enthaltenen Informationen sind von den Benutzern als informativ und rientativ zu verstehen, sowohl hinsichtlich ihres Zwecks als auch ihrer Wirkung, weshalb FLUIDRA nicht für die Genauigkeit dieser Informationen garantiert und daher auch keinerlei Haftung für mögliche Schäden oder Unannehmlichkeiten übernimmt, die den Benutzern aus den darin enthaltenen ngenauigkeiten entstehen können.",

			"_SUBTITOL5": "DATENSCHUTZRICHTLINIE: SCHUTZ VON PERSONENBEZOGENEN DATEN",

			"_AVIS7": "Die Benutzer verpflichten sich, in gutem Glauben auf diese App zuzugreifen und sie zu nutzen.",

			"_AVIS7.1": "In Einhaltung der Bestimmungen aus dem Ausführungsgesetz 15/1999 zum Schutz von personenbezogenen Daten teilen wir Ihnen mit, dass die in die App aufgenommenen personenbezogenen Daten",

			"_AVIS7.2": "1.- mit maximaler Vertraulichkeit behandelt und in Dateien, deren Eigentümerin FLUIDRA S.A ist, zur Verwaltung der App und zur Verwaltung der zwischen ihnen bestehenden Arbeitsbeziehungen aufgenommen werden.",

			"_AVIS7.3": "2.- die Annahme dieser Datenschutzrichtlinie sowie die Genehmigung an FLUIDRA zur Verarbeitung der bereitgestellten personenbezogenen Daten darstellt.",

			"_AVIS7.4": "In Einhaltung der geltenden Gesetze zum Schutz von personenbezogenen Daten hat FLUIDRA angemessene Schutzmaßnahmen für die von den Benutzern bereitgestellten Daten getroffen und außerdem alle verfügbaren Mittel und Maßnahmen eingesetzt, um deren Verlust, Missbrauch, Veränderung, unberechtigten Zugriff auf und Extrahierung der Daten zu verhindern.",

			"_SUBTITOL6": "Recht auf Zugriff, Berichtigung, Löschung und Widerspruch",

			"_AVIS8": "Wie in dem besagten Ausführungsgesetz festgelegt, haben die Benutzer jederzeit das Recht, ihre Rechte auf Zugriff, Berichtigung, Löschung und Widerspruch hinsichtlich ihrer personenbezogenen Daten auszuüben. Hierzu richten sie ein Schreiben unter der E-Mail-Adresse lopd@fluidra.com oder der Postanschrift Avda. Francesc Macià 60, pl. 20 de Sabadell, (Barcelona) an die Personalabteilung. Darin ist anzugeben, welches Recht ausgeübt wird, und eine Kopie des Personalausweises beizulegen.",

			"_SUBTITOL7": "ANLEITUNG ZUR SICHEREN NUTZUNG:",

			"_SUBTITOL8": "Phishing",

			"_AVIS9": "Verwenden Sie keine Links in E-Mails oder auf Websites von Dritten, um auf diese App zuzugreifen. Regelmäßig wird der willkürliche Massenversand von E-Mails festgestellt, die von falschen E-Mail-Adressen zu dem einzigen Zweck gesandt werden, vertrauliche Informationen der enutzer zu erhalten. Diese Technik wird als „ Phishing “ bezeichnet. FLUIDRA lehnt jede Fremdhaftung in diesem Zusammenhang ab.",

			"_SUBTITOL9": "ANWENDBARES RECHT UND GERICHTSSTAND",

			"_AVIS10": "Der Zugriff auf und die Nutzung der App unterliegen spanischem Recht und werden entsprechend ausgelegt. Jeglicher Rechtsstreit, der zwischen FLUIDRA und den Benutzern der App entstehen kann, wird vor den Gerichten der Stadt Barcelona (Spanien) verhandelt, und die Parteien erzichten ausdrücklich auf ihren eigenen Gerichtsstand.",
			//End avis legal

			//Bases
			"_TITLE_BASES": "TEILNAHMEBEDINGUNGEN FÜR DEN WETTBEWERB \"Our New Slogan\"",

			"_BASES1": "Ziel: Im Einklang mit der neuen Mission von Fluidra „Sorge für das Wohlbefinden und die Gesundheit der Menschen durch die nachhaltige Nutzung von Wasser in Freizeit, Sport und Therapie“ besteht das Ziel dieses Wettbewerbs darin, unter allen Mitarbeitern von Fluidra den besten Slogan zu finden, der den Sinn der Mission so ausdrückt, dass er den Menschen wirklich etwas sagt, und damit den bisherigen Slogan „Making Water Perform“ zu ersetzen.",

			"_BASES2": "Thema: Für die Teilnahme am Wettbewerb werden Vorschläge für einen neuen Slogan für die GRUPPE FLUIDRA eingereicht. Dabei wird eine App verwendet, die in den Sprachen Spanisch, Englisch, Französisch, Deutsch und Italienisch zur Verfügung steht.",

			"_BASES3": "Unter einem Slogan (Claim, Motto) versteht sich ein entscheidender Satz oder ein Ausdruck, der den besonderen Wert des Unternehmens ausdrückt. Er muss in der Lage sein, eine überzeugende Botschaft herüberzubringen, die das innerste Wesen des Unternehmens vermittelt.",

			"_BASES4": "Teilnehmer: An diesem Wettbewerb dürfen nur die Mitarbeiter aller Unternehmen der FLUIDRA-GRUPPE teilnehmen. Davon ausgeschlossen sind die Mitarbeiter der Abteilung für Unternehmenskommunikation sowie die Mitglieder der Jury.",

			"_BASES5": "Einschreibung: Die Einschreibung ist kostenlos und erfolgt über die Erstellung eines Benutzerprofils auf der App für den Wettbewerb.",

			"_BASES6": "Annahme von Vorschlägen, Fristen und Voraussetzungen: Es gibt keine Obergrenze für die Anzahl der Vorschläge pro Teilnehmer. Zulässige Sprachen sind Spanisch, Englisch, Französisch, Deutsch und Italienisch.",

			"_BASES7": "Die Veröffentlichung von Vorschlägen ist bis zum 30. September möglich. Parallel können die Teilnehmer beliebig viele Vorschläge anderer Teilnehmer „liken“.",

			"_BASES8": "Die Jury wertet die Vorschläge ab dem 1. Oktober 2016 aus, und im Lauf des Monats Oktober wird der Gewinner-Slogan veröffentlicht.",

			"_BASES9": "Jury: Die Jury setzt sich aus dem Unternehmensausschuss zusammen.",

			"_BASES10": "Kriterien für die Auswahl der Gewinnerprojekte: Die „Likes“ der Teilnehmer werden zwar berücksichtigt, sind aber nicht verbindlich, und die Jury berücksichtigt die folgenden Kriterien bei der Auswahl des Gewinner-Slogans:",

			"_BASES10.1": "- Besonders originelle und kreative Vorschläge, welche die Unternehmenswerte ausdrücken und zur neuen Mission von Fluidra passen: Sorge für das Wohlbefinden und die Gesundheit der Menschen durch. die nachhaltige Nutzung von Wasser in Freizeit, Sport und Therapie.",

			"_BASES10.2": "- Der Gewinnervorschlag muss sinnvoll sein und auf Englisch gut klingen. Wenn die Jury keinen überzeugenden Vorschlag für den Slogan der FLUIDRA- GRUPPE findet, kann sie entscheiden, den Preis nicht zu vergeben. Die FLUIDRA-GRUPPE behält sich das Recht vor, jegliche Änderungen oder Anpassungen am Gewinnervorschlag vorzunehmen.",

			"_BASES11": "Preis: Der Gewinnervorschlag wird in der App, auf der Website livingfluidra.fluidra.com und per E-Mail an den Gewinner veröffentlicht.",

			"_BASES12": "Der Preis ist ein Gutschein für ein Wochenende in einem Wellness-Hotel für zwei Personen im Wert von 1.000 Euro.",

			"_BASES13": "Der Preis wird von einer leitenden Führungskraft von Fluidra überreicht, und der Gewinner wird für die Online-Zeitschrift von Fluidra „LivingFluidra“ interviewt.",

			"_BASES14": "Der Preis kann nicht in Bar ausgezahlt werden.",

			"_BASES15": "Annahme und Disqualifizierung: Das Herunterladen der App für den Wettbewerb und die nachfolgende Erstellung eines Profils auf der App bedeutet die ausdrückliche Annahme aller in diesen Teilnahmebedingungen aufgeführten Punkte und die Annahme des Urteils der Jury, gegen das kein Widerspruch möglich ist. Die Nichteinhaltung dieser Teilnahmebedingungen führt zur Disqualifizierung des Teilnehmers.",

			"_BASES16": "Schutz von personenbezogenen Daten: Gemäß den Bestimmungen der geltenden Gesetze zum Schutz von personenbezogenen Daten weist das Unternehmen FLUIDRA S.A. mit Sitz in Av. Francesc Macià, 60, planta 20, 08208 Sabadell, Barcelona, das zur FLUIDRA-GRUPPE gehört, Sie darauf hin, dass Ihre personenbezogenen Daten in eine Datei aufgenommen wurden, die von und unter der Verantwortung von FLUIDRA S.A. zur Ausübung der Tätigkeit des Unternehmens erstellt wurde.",

			"_BASES17": "Des Weiteren weisen wir Sie darauf hin, dass Ihre personenbezogenen Daten zu den gleichen Zwecken an die einzelnen Unternehmen der FLUIDRA-GRUPPE, die Sie jederzeit über das Intranet des Unternehmens einsehen können, abgetreten werden können, und dass diese Abtretung für die Durchführung, Einhaltung und Kontrolle Ihrer Arbeitsbeziehung mit dem Unternehmen erforderlich ist. Wenn Sie mit Obigem nicht einverstanden sind, können Sie jederzeit Ihre Rechte auf Zugriff, Berichtigung, Löschung und/oder Widerspruch in einem an lopd@fluidra.com gerichteten Schreiben ausüben.",

			"_BASES18": "Eingereichtes Material: Der Teilnehmer erklärt, dass der im Wettbewerb eingereichte Slogan original ist, von ihm selbst kreiert wurde und keinerlei Kopie oder Plagiat umfasst.",

			"_BASES19": "Der Teilnehmer tritt FLUIDRA S.A. vollständig alle Rechte am geistigen und/oder industriellen Eigentum ab, die aus den vorgeschlagenen Slogans entstehen können, unabhängig davon, ob sie gewinnen oder nicht. Die Abtretung, die in diesem Dokument erteilt wird, gilt exklusiv und weltweit.",

			"_BASES20": "Die Dauer der Rechteabtretung erstreckt sich über die maximale, in der geltenden internationalen Gesetzgebung vorgesehene Frist. Die vorliegende exklusive Abtretung umfasst auch alle Nutzungsrechte, die sich aus jeglicher Anpassung oder Änderung ergeben, die aus irgend einem Grund an den Slogans vorgenommen wird.",

			"_BASES21": "FLUIDRA S.A. hat völlig freie Hand bei der Nutzung der Rechte, die Gegenstand dieser Abtretung sind, und kann diese selbst oder über Dritte nutzen, denen vollständige oder teilweise Lizenzen für von FLUIDRA festgelegte Zeiten und Bedingungen erteilt werden können.",
			//End Bases

			//claims
			"Cargando más claims...": "Mehr Slogans laden...",
			//End claims	

			//dash
			"Participar": "Teilnehmen",

			"Mi perfil": "Mein Profil",

			"Eslóganes": "Slogans",

			"Top 5": "Top 5",

			"Bases del concurso": "Teilnahmebedingungen",

			"Aviso legal": "Rechtshinweis",
			//End dash

			//firsttime
			"¡Bienvenido a Fluidra Slogans!": "Willkommen zu Fluidra Slogans!",

			"Queremos explicarte un poco de que va esta app.": "Hier erfahren Sie mehr über diese App.",

			"Participa": "Machen Sie mit!",

			"Desde esta app podrás participar aportando tus ideas para nuestro nuevo slogan. Los mejores se llevaran un buen premio.": "Mit dieser App können Sie sich an der Suche beteiligen und Ihre Ideen für unseren neuen Slogan vorschlagen. Die besten werden prämiert.",

			"Valora": "Bewerten",

			"Valora las publicaciones de otros usuarios dandole al like.": "Bewerten Sie die Vorschläge anderer Nutzer mit Like.",

			"Disfruta": "Spaß haben",

			"Sobretodo, disfruta de esta experiencia.": "Wir möchten, dass Sie bei dieser Aktion Spaß haben.",

			"Continuar": "Weiter",
			//End firsttime

			//login
			"¡Buscamos un nuevo eslogan para Fluidra!": "Wir suchen einen neuen Slogan für Fluidra!",

			"Participa en este emocionante concurso proponiendo eslóganes y valorando los de tus compañeros.": "Machen Sie mit und schlagen Sie Slogans vor und bewerten Sie die Vorschläge Ihrer Kollegen.",

			"Entre todos podemos encontrar el mejor eslogan ¿Será el tuyo? Participa.": "Zusammen finden wir den besten Slogan. Vielleicht ist es Ihrer. Machen Sie mit!",

			"Registrarse": "Anmeldung",

			"Ya estoy registrado": "Ich bin bereits angemeldet",

			"¿Olvidaste la contraseña?": "Kennwort vergessen?",

			"Recuperación de contraseña": "Kennwort wiederherstellen",

			"Escriba el correo electrónico con el que se dió de alta y le enviaremos un enlace para resetear su contraseña.": "Tragen Sie die E-Mail ein, mit der Sie sich angemeldet haben, und wir senden Ihnen einen Link zum Wiederherstellen Ihres Kennworts.",

			"Correo electrónico": "E-Mail",

			"Cancelar": "Abbrechen",

			"Aceptar": "OK",

			"Atención": "Ups!!",

			"Email o contraseña incorrectas.": "E-Mail oder Kennwort nicht korrekt.",

			"Acceder": "Enter",

			"Escriba su correo electrónico y contraseña.": "E-Mail-Adresse und Kennwort eingeben",

			"Contraseña": "Kennwort",
			//End login

			//participa
			"OK": "OK",

			"Escribe un nuevo eslogan...": "Neuen Slogan vorschlagen ...",

			"Publicando...": "Verlag ...",

			"No se ha podido publicar. Intentelo más tarde.": "Es konnte nicht veröffentlichen . Bitte versuchen Sie es später noch einmal.",

			"¡Publicado!": "Posted !",

			"Se ha publicado tu propuesta, muchas gracias por participar.": "Ihr Vorschlag veröffentlicht worden ist, ich danke Ihnen sehr für Ihre Teilnahme.",
			//End participa

			//preauth
			"Para poder usar esta app debes introducir el código de autorización.": "Um diese App nutzen zu können, müssen Sie den Autorisierungscode.",

			"Escanear QR": "",

			"No se ha podido escanear correctamente el código.": "QR-Code konnte nicht korrekt gescannt werden.",

			"Este código no es correcto": "Dieser Code ist nicht korrekt",
			//End preauth

			//preregistre
			"Acepto las condiciones": "Ich erkenne die Bedingungen an",

			"Condiciones y bases del concurso": "Teilnahmebedingungen",
			//End preregistre

			//profile
			"Nombre": "Vorname",

			"Apellidos": "Nachname(n)",

			"Email": "E-Mail",

			"Empresa": "Firma",

			"Idioma": "Sprache",

			"Español": "Español",

			"Italiano": "Italiano",

			"Deutsch": "Deutsch",

			"English": "English",

			"Français": "Français",

			"País": "Land",
			
			"Cerrar sesión" : "Logout",

			"No hemos podido guardado las modificaciones.": "Die Änderungen konnten nicht gespeichert werden.",

			"No hemos podido usar la imagen.": "Das Bild konnte nicht verwendet werden.",

			"¡Guardado!": "Gespeichert!",

			"Se han guardado las modificaciones.": "Die Änderungen wurden gespeichert.",

			"Escoger de la galería": "Aus Bildergalerie auswählen",

			"Camara de fotos": "Kamera",

			"Añadir foto desde...": "Foto hinzufügen aus ...",
			//End profile

			//ranking
			// NO DATA
			//End ranking

			//tabs
			"Añadir": "Hinzufügen"
			//End tabs
		}
	};

	static instance: LocaleFactory;
	static isCreating: boolean = false;
	
	available: Array<String> = [
		'es', 'en', 'it', 'fr', 'de'
	];
	
	currentLang: string;
	constructor() {
		if (!LocaleFactory.isCreating) {
			throw new Error("You can't call new in Singleton instances!");
		}
	}

	static getInstance() {
		if (LocaleFactory.instance == null) {
			LocaleFactory.isCreating = true;
			LocaleFactory.instance = new LocaleFactory();
			LocaleFactory.isCreating = false;
		}

		return LocaleFactory.instance;
	}

	/**
	 * Set the current app's language
	 */
	setCurrentLang(lang: string) {
		this.currentLang = lang;
		console.log(this.currentLang);
	}
	
	/**
	 * Return the current Language
	 */
	getCurrentLang() {
		return this.currentLang;
	}

	/**
	 * Return string locales
	 */
	getLocales() {
		return this.locales[this.currentLang];
	}
	
	/**
	 * Returns all available locales
	 */
	getAvailableLocales() {
		return this.available;
	}
	
	/**
	 * Check if this lang is supported
	 */
	isSuported(lang: string) {
		let found = false;
		this.available.forEach(v => {
			if(!found) found = (v == lang);
		});
		
		
		return found;
	}

	/**
	 * 
	 * @param {string} lang [description]
	 */
	getLocalesByLang(lang: string) {
		return this.locales[lang];
	}

}