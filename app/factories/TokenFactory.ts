
/**
 * Token used with all services...
 */
export class TokenFactory {
	
	static instance: TokenFactory;
	static isCreating: boolean = false;

	token: string;
	
	constructor() {
		if (!TokenFactory.isCreating) {
			throw new Error("You can't call new in Singleton instances!");
		}
	}

	static getInstance() {
		if(TokenFactory.instance == null) {
			TokenFactory.isCreating = true;
			TokenFactory.instance = new TokenFactory();
			TokenFactory.isCreating = false;
		}

		return TokenFactory.instance;
	}

	setToken(t: string) {
        this.token = t;
    }
    
    getToken() {
        return this.token;
    }
}